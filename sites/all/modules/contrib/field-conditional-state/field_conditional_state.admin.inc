<?php
/**
 * @file
 * Containing the backend functions.
 */

/**
 * Returns the available states for a given field type
 *
 * @param string $type
 *   the field type
 */
function field_conditional_state_get_field_states($type) {
  return array('enabled',
    'disabled',
    'required',
    'optional',
    'visible',
    'invisible',
  );
}

/**
 * Returns the triggerable states for a given field type.
 *
 * @param string $type
 *   the field type
 */
function field_conditional_state_get_trigger_states($type) {
  $ret = array();
  switch ($type) {
    case 'list_boolean':
      $ret = array('checked', 'unchecked');
      break;

    default:
      $ret = array('empty', 'filled', 'value');
  }

  return $ret;
}

/**
 * Builds the settings form for a particular field instance.
 *
 * @param array $form
 *   the complete form
 * @param array $form_state
 *   the current form_state
 * @param array $instance
 *   the field instance for which the form is build
 */
function field_conditional_state_settings_form($form, $form_state, $instance) {
  $fields = field_info_instances($instance['entity_type'], $instance['bundle']);
  $current_field = field_info_field($instance['field_name']);

  $dependees = array();
  $dependee_options = array();
  foreach ($fields as $name => $data) {
    if ($name != $instance['field_name']) {
      $dependees[$name] = field_info_field($data['field_name']);
      $dependee_options[$name] = $data['label'];
    }
  }

  $form['conditions'] = array(
    '#prefix' => '<table><tr><th></th><th>State</th><th>Dependee</th><th colspan="2">Trigger</th></tr>',
    '#suffix' => '</table>',
  );

  $possible_states = field_conditional_state_get_field_states($current_field['type']);
  $args = array(
    ':type' => $instance['entity_type'],
    ':bundle' => $instance['bundle'],
    ':field' => $instance['field_name'],
  );
  $conditions = db_query('SELECT * FROM field_conditional_states WHERE entity_type = :type AND bundle = :bundle AND field_name = :field', $args);

  $existing_ids = array();
  while ($condition = $conditions->fetchAssoc()) {
    $trigger_value = unserialize($condition['trigger_values']);
    $trigger_state = key($trigger_value);
    $existing_ids[] = $id = $condition['id'];

    $current = &$form['conditions'][];
    $current = array('#prefix' => '<tr><td>#' . $id . '</td>', '#suffix' => '</tr>');
    $current['state' . $id] = array(
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#type' => 'select',
      '#title' => t('State'),
      '#required' => TRUE,
      '#options' => drupal_map_assoc($possible_states),
      '#default_value' => $condition['state'],
    );
    $current['control_field' . $id] = array(
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#type' => 'select',
      '#title' => t('Dependee'),
      '#ajax' => array(
        'event' => 'change',
        'wrapper' => 'field-conditional-state-settings-form',
        'callback' => 'field_conditional_state_get_trigger_values',
        'method' => 'replace',
      ),
      '#required' => TRUE,
      '#default_value' => $condition['control_field'],
      '#options' => $dependee_options,
    );

    $current['trigger' . $id] = array(
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#type' => 'select',
      '#title' => t('Trigger'),
      '#required' => TRUE,
      '#default_value' => $trigger_state,
      '#options' => drupal_map_assoc(field_conditional_state_get_trigger_states($dependees[$condition['control_field']]['type'])),
    );

    if (!empty($form_state['values']['control_field' . $id])) {
      $current['trigger' . $id]['#options'] = drupal_map_assoc(field_conditional_state_get_trigger_states($dependees[$form_state['values']['control_field' . $id]]['type']));
      $current['control_field' . $id]['#default_value'] = $form_state['values']['control_field' . $id];
    }
    $current['value_text' . $id] = array(
      '#type' => 'textfield',
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#title' => t('Value'),
      '#default_value' => ($trigger_state == 'value' ? $trigger_value[$trigger_state] : ""),
      '#states' => array(
        'visible' => array(
          ':input[name="trigger' . $id . '"]' => array('value' => 'value'),
        ),
      ),
    );
  }

  $add_new = &$form['conditions']['add_new'];
  $add_new = array('#prefix' => '<tr id="fcs_new_condition"><td>New</td>', '#suffix' => '</tr>');
  $add_new['state'] = array(
    '#prefix' => '<td>',
    '#suffix' => '</td>',
    '#type' => 'select',
    '#title' => t('State'),
    '#empty_option' => t('-Select-'),
    '#options' => drupal_map_assoc($possible_states),
    '#states' => array(
      'optional' => array(
        ':input[name="control_field"]' => array('value' => ''),
        ':input[name="trigger"]' => array('value' => ''),
      ),
    ),
  );
  $add_new['control_field'] = array(
    '#prefix' => '<td>',
    '#suffix' => '</td>',
    '#type' => 'select',
    '#title' => t('Dependee'),
    '#empty_option' => t('-Select-'),
    '#ajax' => array(
      'event' => 'change',
      'wrapper' => 'field-conditional-state-settings-form',
      'callback' => 'field_conditional_state_get_trigger_values',
      'method' => 'replace',
    ),
    '#states' => array(
      'optional' => array(
        ':input[name="state"]' => array('value' => ''),
        ':input[name="trigger"]' => array('value' => ''),
      ),
    ),
    '#default_value' => (!empty($form_state['values']['control_field']) ? $form_state['values']['control_field'] : NULL),
    '#options' => $dependee_options,
  );

  $add_new['trigger'] = array(
    '#prefix' => '<td>',
    '#suffix' => '</td>',
    '#empty_option' => t('-Select-'),
    '#type' => 'select',
    '#title' => t('Trigger'),
    '#states' => array(
      'optional' => array(
        ':input[name="state"]' => array('value' => ''),
        ':input[name="control_field"]' => array('value' => ''),
      ),
    ),
    '#options' => array(),
  );

  $add_new['value_text'] = array(
    '#type' => 'textfield',
    '#prefix' => '<td>',
    '#suffix' => '</td>',
    '#title' => t('Value'),
    '#states' => array(
      'visible' => array(
        ':input[name="trigger"]' => array('value' => 'value'),
      ),
    ),
  );
  if (!empty($form_state['values']['control_field'])) {
    $add_new['trigger']['#options'] = drupal_map_assoc(field_conditional_state_get_trigger_states($dependees[$form_state['values']['control_field']]['type']));
  }

  $form['existing_ids'] = array(
    '#type' => 'hidden',
    '#value' => serialize($existing_ids),
  );

  $form['done'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

/**
 * Handles the submitted data of the settings form.
 *
 * @param array $form
 *   the complete form
 * @param array $form_state
 *   the current form_state
 */
function field_conditional_state_settings_form_submit($form, $form_state) {
  $instance = $form_state['build_info']['args'][0];
  $values = $form_state['values'];
  $existing_ids = unserialize($values['existing_ids']);

  foreach ($existing_ids as $id) {
    $trigger_value = array($values['trigger' . $id] => ($values['trigger' . $id] == 'value' ? $values['value_text' . $id] : TRUE));
    $data = array(
      'control_field' => $values['control_field' . $id],
      'state' => $values['state' . $id],
      'trigger_values' => serialize($trigger_value),
    );

    $stmt = db_update('field_conditional_states');
    $stmt->fields($data);
    $stmt->condition('id', $id);
    $stmt->execute();
  }

  if (!empty($values['state']) && !empty($values['control_field']) && !empty($values['trigger'])) {
    $trigger_value = array($values['trigger'] => ($values['trigger'] == 'value' ? $values['value_text'] : TRUE));
    $data = array(
      'entity_type' => $instance['entity_type'],
      'bundle' => $instance['bundle'],
      'state' => $values['state'],
      'field_name' => $instance['field_name'],
      'control_field' => $values['control_field'],
      'trigger_values' => serialize($trigger_value),
    );

    $stmt = db_insert('field_conditional_states');
    $stmt->fields($data);
    $stmt->execute();
  }
}
