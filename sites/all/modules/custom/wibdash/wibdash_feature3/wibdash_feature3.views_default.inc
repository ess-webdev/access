<?php
/**
 * @file
 * wibdash_feature3.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wibdash_feature3_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'wibdash';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'wibdash';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Wibdash';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_wibdash_status' => 'field_wibdash_status',
    'field_wibdash_priority' => 'field_wibdash_priority',
    'field_wibdash_type' => 'field_wibdash_type',
    'field_wibdash_category' => 'field_wibdash_category',
    'created' => 'created',
    'field_wibdash_assigned_to' => 'field_wibdash_assigned_to',
    'changed' => 'changed',
  );
  $handler->display->display_options['style_options']['default'] = 'field_wibdash_priority';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_wibdash_status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_wibdash_priority' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_wibdash_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_wibdash_category' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_wibdash_assigned_to' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Status */
  $handler->display->display_options['fields']['field_wibdash_status']['id'] = 'field_wibdash_status';
  $handler->display->display_options['fields']['field_wibdash_status']['table'] = 'field_data_field_wibdash_status';
  $handler->display->display_options['fields']['field_wibdash_status']['field'] = 'field_wibdash_status';
  $handler->display->display_options['fields']['field_wibdash_status']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_wibdash_status']['alter']['text'] = '[field_wibdash_status] <a href="wibdash/action/incstat/[nid]">u</a>';
  /* Field: Content: Priority */
  $handler->display->display_options['fields']['field_wibdash_priority']['id'] = 'field_wibdash_priority';
  $handler->display->display_options['fields']['field_wibdash_priority']['table'] = 'field_data_field_wibdash_priority';
  $handler->display->display_options['fields']['field_wibdash_priority']['field'] = 'field_wibdash_priority';
  /* Field: Content: Type */
  $handler->display->display_options['fields']['field_wibdash_type']['id'] = 'field_wibdash_type';
  $handler->display->display_options['fields']['field_wibdash_type']['table'] = 'field_data_field_wibdash_type';
  $handler->display->display_options['fields']['field_wibdash_type']['field'] = 'field_wibdash_type';
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_wibdash_category']['id'] = 'field_wibdash_category';
  $handler->display->display_options['fields']['field_wibdash_category']['table'] = 'field_data_field_wibdash_category';
  $handler->display->display_options['fields']['field_wibdash_category']['field'] = 'field_wibdash_category';
  $handler->display->display_options['fields']['field_wibdash_category']['element_label_colon'] = FALSE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Created';
  $handler->display->display_options['fields']['created']['date_format'] = 'raw time ago';
  /* Field: Content: Assigned to */
  $handler->display->display_options['fields']['field_wibdash_assigned_to']['id'] = 'field_wibdash_assigned_to';
  $handler->display->display_options['fields']['field_wibdash_assigned_to']['table'] = 'field_data_field_wibdash_assigned_to';
  $handler->display->display_options['fields']['field_wibdash_assigned_to']['field'] = 'field_wibdash_assigned_to';
  /* Field: Content: Updated date */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'node';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Last updated';
  $handler->display->display_options['fields']['changed']['date_format'] = 'raw time ago';
  /* Sort criterion: Content: Status (field_wibdash_status) */
  $handler->display->display_options['sorts']['field_wibdash_status_value']['id'] = 'field_wibdash_status_value';
  $handler->display->display_options['sorts']['field_wibdash_status_value']['table'] = 'field_data_field_wibdash_status';
  $handler->display->display_options['sorts']['field_wibdash_status_value']['field'] = 'field_wibdash_status_value';
  /* Sort criterion: Content: Priority (field_wibdash_priority) */
  $handler->display->display_options['sorts']['field_wibdash_priority_value']['id'] = 'field_wibdash_priority_value';
  $handler->display->display_options['sorts']['field_wibdash_priority_value']['table'] = 'field_data_field_wibdash_priority';
  $handler->display->display_options['sorts']['field_wibdash_priority_value']['field'] = 'field_wibdash_priority_value';
  $handler->display->display_options['sorts']['field_wibdash_priority_value']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'wibdash_item' => 'wibdash_item',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Status (field_wibdash_status) */
  $handler->display->display_options['filters']['field_wibdash_status_value']['id'] = 'field_wibdash_status_value';
  $handler->display->display_options['filters']['field_wibdash_status_value']['table'] = 'field_data_field_wibdash_status';
  $handler->display->display_options['filters']['field_wibdash_status_value']['field'] = 'field_wibdash_status_value';
  $handler->display->display_options['filters']['field_wibdash_status_value']['operator'] = 'not';
  $handler->display->display_options['filters']['field_wibdash_status_value']['value'] = array(
    'all' => 'all',
    'Not started' => 'Not started',
    'In progress' => 'In progress',
    'Waiting' => 'Waiting',
    'Done' => 'Done',
  );
  $handler->display->display_options['filters']['field_wibdash_status_value']['group'] = 1;
  $handler->display->display_options['filters']['field_wibdash_status_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_wibdash_status_value']['expose']['operator_id'] = 'field_wibdash_status_value_op';
  $handler->display->display_options['filters']['field_wibdash_status_value']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['field_wibdash_status_value']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_wibdash_status_value']['expose']['operator'] = 'field_wibdash_status_value_op';
  $handler->display->display_options['filters']['field_wibdash_status_value']['expose']['identifier'] = 'field_wibdash_status_value';
  $handler->display->display_options['filters']['field_wibdash_status_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: Priority (field_wibdash_priority) */
  $handler->display->display_options['filters']['field_wibdash_priority_value']['id'] = 'field_wibdash_priority_value';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['table'] = 'field_data_field_wibdash_priority';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['field'] = 'field_wibdash_priority_value';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['group'] = 1;
  $handler->display->display_options['filters']['field_wibdash_priority_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_wibdash_priority_value']['expose']['operator_id'] = 'field_wibdash_priority_value_op';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['expose']['label'] = 'Priority';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['expose']['operator'] = 'field_wibdash_priority_value_op';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['expose']['identifier'] = 'field_wibdash_priority_value';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_wibdash_priority_value']['group_info']['label'] = 'Priority (field_wibdash_priority)';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['group_info']['identifier'] = 'field_wibdash_priority_value';
  $handler->display->display_options['filters']['field_wibdash_priority_value']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['field_wibdash_priority_value']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );
  /* Filter criterion: Content: Type (field_wibdash_type) */
  $handler->display->display_options['filters']['field_wibdash_type_value']['id'] = 'field_wibdash_type_value';
  $handler->display->display_options['filters']['field_wibdash_type_value']['table'] = 'field_data_field_wibdash_type';
  $handler->display->display_options['filters']['field_wibdash_type_value']['field'] = 'field_wibdash_type_value';
  $handler->display->display_options['filters']['field_wibdash_type_value']['group'] = 1;
  $handler->display->display_options['filters']['field_wibdash_type_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_wibdash_type_value']['expose']['operator_id'] = 'field_wibdash_type_value_op';
  $handler->display->display_options['filters']['field_wibdash_type_value']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['field_wibdash_type_value']['expose']['operator'] = 'field_wibdash_type_value_op';
  $handler->display->display_options['filters']['field_wibdash_type_value']['expose']['identifier'] = 'field_wibdash_type_value';
  $handler->display->display_options['filters']['field_wibdash_type_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: Category (field_wibdash_category) */
  $handler->display->display_options['filters']['field_wibdash_category_value']['id'] = 'field_wibdash_category_value';
  $handler->display->display_options['filters']['field_wibdash_category_value']['table'] = 'field_data_field_wibdash_category';
  $handler->display->display_options['filters']['field_wibdash_category_value']['field'] = 'field_wibdash_category_value';
  $handler->display->display_options['filters']['field_wibdash_category_value']['group'] = 1;
  $handler->display->display_options['filters']['field_wibdash_category_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_wibdash_category_value']['expose']['operator_id'] = 'field_wibdash_category_value_op';
  $handler->display->display_options['filters']['field_wibdash_category_value']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['field_wibdash_category_value']['expose']['operator'] = 'field_wibdash_category_value_op';
  $handler->display->display_options['filters']['field_wibdash_category_value']['expose']['identifier'] = 'field_wibdash_category_value';
  $handler->display->display_options['filters']['field_wibdash_category_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: Assigned to (field_wibdash_assigned_to) */
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['id'] = 'field_wibdash_assigned_to_uid';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['table'] = 'field_data_field_wibdash_assigned_to';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['field'] = 'field_wibdash_assigned_to_uid';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['group'] = 1;
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['expose']['operator_id'] = 'field_wibdash_assigned_to_uid_op';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['expose']['label'] = 'Assigned to';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['expose']['operator'] = 'field_wibdash_assigned_to_uid_op';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['expose']['identifier'] = 'field_wibdash_assigned_to_uid';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['group_info']['label'] = 'Assigned to (field_wibdash_assigned_to)';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['group_info']['identifier'] = 'field_wibdash_assigned_to_uid';
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['field_wibdash_assigned_to_uid']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );
  /* Filter criterion: Content: Has taxonomy term */
  $handler->display->display_options['filters']['tid']['id'] = 'tid';
  $handler->display->display_options['filters']['tid']['table'] = 'taxonomy_index';
  $handler->display->display_options['filters']['tid']['field'] = 'tid';
  $handler->display->display_options['filters']['tid']['value'] = '';
  $handler->display->display_options['filters']['tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['tid']['expose']['operator_id'] = 'tid_op';
  $handler->display->display_options['filters']['tid']['expose']['label'] = 'Tags';
  $handler->display->display_options['filters']['tid']['expose']['operator'] = 'tid_op';
  $handler->display->display_options['filters']['tid']['expose']['identifier'] = 'tid';
  $handler->display->display_options['filters']['tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['tid']['reduce_duplicates'] = TRUE;
  $handler->display->display_options['filters']['tid']['vocabulary'] = 'wibdash_tags';

  /* Display: Wibdash */
  $handler = $view->new_display('page', 'Wibdash', 'Wibdash');
  $handler->display->display_options['path'] = 'wibdash';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Wibdash';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['wibdash'] = $view;

  return $export;
}
