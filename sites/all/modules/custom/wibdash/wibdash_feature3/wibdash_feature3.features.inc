<?php
/**
 * @file
 * wibdash_feature3.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wibdash_feature3_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wibdash_feature3_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wibdash_feature3_node_info() {
  $items = array(
    'wibdash_item' => array(
      'name' => t('Wibdash Item'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
