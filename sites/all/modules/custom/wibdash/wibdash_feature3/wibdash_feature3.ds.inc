<?php
/**
 * @file
 * wibdash_feature3.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function wibdash_feature3_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|wibdash_item|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'wibdash_item';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '17',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|wibdash_item|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function wibdash_feature3_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|wibdash_item|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'wibdash_item';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'left' => array(
        1 => 'field_wibdash_origin',
        2 => 'field_wibdash_description',
        3 => 'field_wibdash_status_comment',
        4 => 'field_wibdash_attach_files',
        5 => 'field_wibdash_time_estimation',
        6 => 'field_wibdash_time_spent',
        7 => 'field_wibdash_notifications',
      ),
      'right' => array(
        8 => 'field_wibdash_assigned_to',
        9 => 'field_wibdash_due_date',
        10 => 'field_wibdash_priority',
        11 => 'field_wibdash_type',
        12 => 'field_wibdash_category',
        13 => 'field_wibdash_status',
        14 => 'field_wibdash_external_link',
        15 => 'field_wibdash_tags',
      ),
      'footer' => array(
        16 => 'field_wibdash_system_information',
        17 => 'comments',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'field_wibdash_origin' => 'left',
      'field_wibdash_description' => 'left',
      'field_wibdash_status_comment' => 'left',
      'field_wibdash_attach_files' => 'left',
      'field_wibdash_time_estimation' => 'left',
      'field_wibdash_time_spent' => 'left',
      'field_wibdash_notifications' => 'left',
      'field_wibdash_assigned_to' => 'right',
      'field_wibdash_due_date' => 'right',
      'field_wibdash_priority' => 'right',
      'field_wibdash_type' => 'right',
      'field_wibdash_category' => 'right',
      'field_wibdash_status' => 'right',
      'field_wibdash_external_link' => 'right',
      'field_wibdash_tags' => 'right',
      'field_wibdash_system_information' => 'footer',
      'comments' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|wibdash_item|full'] = $ds_layout;

  return $export;
}
