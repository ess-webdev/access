<?php
/**
 * @file
 * chess_file_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function chess_file_feature_node_info() {
  $items = array(
    'chess_file' => array(
      'name' => t('chess_file'),
      'base' => 'node_content',
      'description' => t('Stores information such as url, title etc. about CHESS files'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
