<?php
/**
 * @file
 * chess_view_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function chess_view_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'chess_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'chess_view';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Chess files';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_chess_fileurl' => 'field_chess_fileurl',
    'field_chess_files' => 'field_chess_files',
    'field_chess_description' => 'field_chess_description',
    'field_chess_modified' => 'field_chess_modified',
    'field_chess_author' => 'field_chess_author',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_fileurl' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_files' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_description' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_modified' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_author' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: fileurl */
  $handler->display->display_options['fields']['field_chess_fileurl']['id'] = 'field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['table'] = 'field_data_field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['field'] = 'field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_chess_fileurl']['alter']['make_link'] = TRUE;
  /* Field: Content: files */
  $handler->display->display_options['fields']['field_chess_files']['id'] = 'field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['table'] = 'field_data_field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['field'] = 'field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['label'] = 'Files';
  $handler->display->display_options['fields']['field_chess_files']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_chess_files']['alter']['path'] = 'sites/default/files/private/chess/[field_chess_fileurl]/[field_chess_files]';
  $handler->display->display_options['fields']['field_chess_files']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_chess_files']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_chess_files']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_chess_files']['separator'] = '<br/>';
  /* Field: Content: description */
  $handler->display->display_options['fields']['field_chess_description']['id'] = 'field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['table'] = 'field_data_field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['field'] = 'field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['label'] = 'Description';
  /* Field: Content: modified */
  $handler->display->display_options['fields']['field_chess_modified']['id'] = 'field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['table'] = 'field_data_field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['field'] = 'field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['label'] = 'Last modified';
  /* Field: Content: author */
  $handler->display->display_options['fields']['field_chess_author']['id'] = 'field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['table'] = 'field_data_field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['field'] = 'field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['label'] = 'Author';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'chess_file' => 'chess_file',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'allwords';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: files (field_chess_files) */
  $handler->display->display_options['filters']['field_chess_files_value']['id'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value']['table'] = 'field_data_field_chess_files';
  $handler->display->display_options['filters']['field_chess_files_value']['field'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value']['operator'] = 'allwords';
  $handler->display->display_options['filters']['field_chess_files_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_files_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['operator_id'] = 'field_chess_files_value_op';
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['label'] = 'Filename';
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['operator'] = 'field_chess_files_value_op';
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['identifier'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_chess_files_value']['group_info']['label'] = 'files (field_chess_files)';
  $handler->display->display_options['filters']['field_chess_files_value']['group_info']['identifier'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['field_chess_files_value']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );
  /* Filter criterion: Content: author (field_chess_author) */
  $handler->display->display_options['filters']['field_chess_author_value']['id'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['table'] = 'field_data_field_chess_author';
  $handler->display->display_options['filters']['field_chess_author_value']['field'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['operator'] = 'allwords';
  $handler->display->display_options['filters']['field_chess_author_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_author_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['operator_id'] = 'field_chess_author_value_op';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['label'] = 'Author';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['operator'] = 'field_chess_author_value_op';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['identifier'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: files (field_chess_files) */
  $handler->display->display_options['filters']['field_chess_files_value_1']['id'] = 'field_chess_files_value_1';
  $handler->display->display_options['filters']['field_chess_files_value_1']['table'] = 'field_data_field_chess_files';
  $handler->display->display_options['filters']['field_chess_files_value_1']['field'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value_1']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_files_value_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_files_value_1']['expose']['operator_id'] = 'field_chess_files_value_1_op';
  $handler->display->display_options['filters']['field_chess_files_value_1']['expose']['label'] = 'files (field_chess_files)';
  $handler->display->display_options['filters']['field_chess_files_value_1']['expose']['operator'] = 'field_chess_files_value_1_op';
  $handler->display->display_options['filters']['field_chess_files_value_1']['expose']['identifier'] = 'field_chess_files_value_1';
  $handler->display->display_options['filters']['field_chess_files_value_1']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['field_chess_files_value_1']['group_info']['label'] = 'Filetype';
  $handler->display->display_options['filters']['field_chess_files_value_1']['group_info']['identifier'] = 'field_chess_files_value_1';
  $handler->display->display_options['filters']['field_chess_files_value_1']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'pdf',
      'operator' => '=',
      'value' => '%.pdf',
    ),
    2 => array(
      'title' => 'docx',
      'operator' => '=',
      'value' => '%.docx',
    ),
    3 => array(
      'title' => 'pptx',
      'operator' => '=',
      'value' => '%.pptx',
    ),
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'chess-view';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'chess_view';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['chess_view'] = $view;

  return $export;
}
