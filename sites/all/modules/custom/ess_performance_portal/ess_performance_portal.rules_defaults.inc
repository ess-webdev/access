<?php
/**
 * @file
 * ess_performance_portal.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ess_performance_portal_default_rules_configuration() {
  $items = array();
  $items['rules_clear_cache_views'] = entity_import('rules_config', '{ "rules_clear_cache_views" : {
      "LABEL" : "Clear cache views",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : { "node_presave--planner_table" : { "bundle" : "planner_table" } },
      "IF" : [
        { "NOT entity_is_new" : { "entity" : [ "node" ] } },
        { "AND" : [
            { "NOT data_is" : { "data" : [ "node" ], "value" : [ "node-unchanged" ] } }
          ]
        }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "node:field-planner-vr-date" ],
            "value" : [ "site:current-date" ]
          }
        },
        { "data_set" : {
            "data" : [ "node:field-planner-vr-author" ],
            "value" : [ "site:current-user" ]
          }
        },
        { "cache_actions_action_clear_views_cache" : { "view" : { "value" : {
                "total_year_overview" : "total_year_overview",
                "vrs_provided" : "vrs_provided",
                "collection_of_vrs" : "collection_of_vrs"
              }
            }
          }
        }
      ]
    }
  }');
  $items['rules_clear_cache_views_after_import'] = entity_import('rules_config', '{ "rules_clear_cache_views_after_import" : {
      "LABEL" : "Clear cache views after import",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "cache_actions", "feeds" ],
      "ON" : { "feeds_after_import" : [] },
      "IF" : [ { "NOT AND" : [] } ],
      "DO" : [
        { "cache_actions_action_clear_views_cache" : { "view" : { "value" : {
                "total_year_overview" : "total_year_overview",
                "collection_of_vrs" : "collection_of_vrs",
                "vrs_provided" : "vrs_provided"
              }
            }
          }
        }
      ]
    }
  }');
  return $items;
}
