<?php
/**
 * @file
 * ess_performance_portal.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ess_performance_portal_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels" && $api == "layouts") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ess_performance_portal_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ess_performance_portal_node_info() {
  $items = array(
    'planner_table' => array(
      'name' => t('Control account'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'project_period' => array(
      'name' => t('Project period'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('ID'),
      'help' => '',
    ),
    'project_year' => array(
      'name' => t('Project year'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('ID'),
      'help' => '',
    ),
    'project_year_data' => array(
      'name' => t('Project year data'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
