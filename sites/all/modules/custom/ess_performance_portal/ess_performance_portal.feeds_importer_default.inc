<?php
/**
 * @file
 * ess_performance_portal.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ess_performance_portal_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'epp_project_year_curve';
  $feeds_importer->config = array(
    'name' => 'Feeds EPP project year curve',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'ac',
            'target' => 'field_project_year_data_ac',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'pv',
            'target' => 'field_project_year_data_pv',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'ev',
            'target' => 'field_project_year_data_ev',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'ID',
            'target' => 'title',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'projectcode',
            'target' => 'field_project_year_data_pc',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'year',
            'target' => 'field_project_year_data_year',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'month',
            'target' => 'field_project_year_data_rel_mont:label',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'month',
            'target' => 'field_project_year_data_month',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'year',
            'target' => 'field_project_year_data_rel_thre:label',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'dispyear',
            'target' => 'field_project_year_data_dispyear',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'curr_year_month',
            'target' => 'field_project_year_data_curr_yyy',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'project_year_data',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '86400',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['epp_project_year_curve'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'feeds_epp_control_account';
  $feeds_importer->config = array(
    'name' => 'Feeds EPP control account',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'projectcacode',
            'target' => 'field_field_planner_projectcacod',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'projectcacode',
            'target' => 'field_planner_cacode',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'projectname',
            'target' => 'field_planner_projectname',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'projectcode',
            'target' => 'field_planner_projectcode',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'year',
            'target' => 'field_planner_year',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'year',
            'target' => 'field_planner_rel_threshold:label',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'month',
            'target' => 'field_planner_month',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'month',
            'target' => 'field_planner_rel_month:label',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'id',
            'target' => 'title',
            'unique' => 1,
          ),
          9 => array(
            'source' => 'pv',
            'target' => 'field_planner_pv',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'ev',
            'target' => 'field_planner_ev',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'ac',
            'target' => 'field_planner_ac',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'cum_ac',
            'target' => 'field_planner_cum_ac',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'cum_pv',
            'target' => 'field_planner_cum_pv',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'cum_ev',
            'target' => 'field_planner_cum_ev',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'planner_table',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['feeds_epp_control_account'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'feeds_epp_project_period';
  $feeds_importer->config = array(
    'name' => 'Feeds EPP project period',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'year',
            'target' => 'field_project_period_year',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'month',
            'target' => 'field_project_period_month',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'projectcode',
            'target' => 'field_project_period_code',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'id',
            'target' => 'title',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'file_list',
            'target' => 'field_project_period_files_list',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'ev',
            'target' => 'field_project_period_ev',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'ac',
            'target' => 'field_project_period_ac',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'pv',
            'target' => 'field_project_period_pv',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'month',
            'target' => 'field_project_period_rel_month:label',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'projectname',
            'target' => 'field_project_period_name',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'cum_pv',
            'target' => 'field_project_period_cum_pv',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'cum_ac',
            'target' => 'field_project_period_cum_ac',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'cum_ev',
            'target' => 'field_project_period_cum_ev',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'year',
            'target' => 'field_project_period_rel_thresho:label',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'project_period',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['feeds_epp_project_period'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'feeds_epp_project_year';
  $feeds_importer->config = array(
    'name' => 'Feeds EPP project year',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'ID',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'ev',
            'target' => 'field_project_year_ev',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'ac',
            'target' => 'field_project_year_ac',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'pv',
            'target' => 'field_project_year_pv',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'year',
            'target' => 'field_project_year_year',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'month',
            'target' => 'field_project_year_month',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'month',
            'target' => 'field_project_year_rel_month:label',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'project_year',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['feeds_epp_project_year'] = $feeds_importer;

  return $export;
}
