(function ($) {
  Drupal.behaviors.autocompleteDisableNid = {
    attach: function (context, settings) {    
      $('.form-autocomplete').each(function() {
		var ac = $(this);
		ac.data('changed', false);
		ac.data('alias', 'ac-real-');
		ac.data('rx', / \([0-9]+\)/);
		ac.data('name', ac.attr('name'));
		ac.data('id', ac.attr('id'));
		ac.data('value', ac.val());
		ac.before('<input name="'+ac.data('alias')+ac.data('name')+'" type="hidden" id="'+ac.data('alias')+ac.data('id')+'" value="'+ac.data('value')+'" />');
		ac.data('real', '#'+ac.data('alias')+ac.data('id'));
	});
	$('.form-autocomplete').blur(function() {
		var ac = $(this);
		var real = $(ac.data('real'));
		if ($.browser.msie && !ac.data('changed')) {
			ac.change();
		}
		if (ac.val().match(ac.data('rx'))) {
			real.val(ac.val());
		}
		ac.val(ac.val().replace(ac.data('rx'), ''));
		ac.attr('name', ac.data('alias')+ac.data('name'));
		real.attr('name', ac.data('name'));
		ac.data('changed', false);
	});
	$('.form-autocomplete').change(function() {
		var ac = $(this);
		if ($.browser.msie) {
			ac.data('changed', true);
		}
		ac.blur();
	});
	$('.form-autocomplete').focus(function() {
		var ac = $(this);
		var real = $(ac.data('real'));
		ac.attr('name', ac.data('name'));
		real.attr('name', ac.data('alias')+ac.data('name'));
	});
	$('.form-autocomplete').each(function() {
		var ac = $(this);
		ac.blur();
	});
    }
  };
})(jQuery);
