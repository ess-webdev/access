<?php
/**
 * @file
 * Contains node hooks and alterations.
 */

/**
 * Implements hook_form_alter().
 *
 * Add relationship to the node group when creating new content.
 */
function access_hooks_form_alter(&$form, &$form_state, $form_id) {
  if (arg(0) == 'node' && arg(1) == 'add') {
    // If group reference is in the URL set the nid as reference.
    if (isset($_GET['og_group_ref'])) {
      // Load node from nid in the URL.
      $node = node_load($_GET['og_group_ref']);

      // Wrap title in double quotes.
      $title = '"' . $node->title . ' (' . $node->nid . ')' . '"';

      // Set the node as group reference.
      $form['og_group_ref']['und']['0']['default']['#default_value'] = $title;
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Set default values for node add group form. This is only done for non admin
 * users.
 */
function access_hooks_form_group_node_form_alter(&$form, &$form_state) {
  global $user;
  if (!in_array('administrator', $user->roles)) {

    // Set default values for when a non administor user is createing a group.
    // but don't do this when we edit a node
    if(!isset($form_state['node']->nid)) {
        // Create a new EntityFieldQuery object.
        $query = new EntityFieldQuery();

        // Get the global node.
        $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'group')
          ->propertyCondition('status', 1)
          ->fieldCondition('field_group_global', 'value', '1', '=');

        $result = $query->execute();

        // If the node exists (which it always should). Set that as the as the head
        // group.
        if (is_array($result['node'])) {
          $global =  node_load(key($result['node']));
          $form['field_head_group']['und']['0']['target_id']['#default_value'] = "$global->title ($global->nid)";
          $form['field_head_group']['und']['#access'] = FALSE;
        }
        
        $form['group_access']['und']['#default_value'] = 1;
        $form['field_head_sub_group']['und']['#default_value'] = 1;
    }

    // Restrict access for group related fields for non administor users.
    $form['field_head_sub_group']['und']['#access'] = FALSE;
    $form['field_group_frontpage']['und']['#access'] = FALSE;
    $form['group_access']['#access'] = FALSE;
    $form['og_roles_permissions']['#access'] = FALSE;
    $form['field_group_global']['#access'] = FALSE;
  }
}
