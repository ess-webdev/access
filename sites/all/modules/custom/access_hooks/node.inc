<?php
/**
 * @file
 * Contains node hooks and alterations.
 */

/**
 * Implements hook_node_alter().
 *
 * This will alter the node when the user is saving a new node or
 * updating a exsisting node.
 */
function access_hooks_node_presave($node) {
  if ($node->type == 'group') {
    // Check if it is a private group that is being created.
    if ($node->group_access['und'][0]['value']) {

      // Get the global group nid.
      $global_nid = db_query("SELECT entity_id FROM {field_data_field_group_global} WHERE field_group_global_value = 1")->fetchField();

      // If there is a group nid, refer the created group to the global one.
      if (is_numeric($global_nid)) {
        $node->field_head_sub_group['und'][0]['value'] = 1;
        $node->field_head_group['und'][0]['target_id'] = $global_nid;
      }
      else {
        // There is no global group created. Return an error.
        form_set_error('field_group_global', t('You have to create a global group before you can create a private one.'));
      }
    }
  }
}

/**
 * Implements hook_node_validate().
 *
 * This will alter the node when the user is saving a new node or
 * updating a exsisting node.
 */
function access_hooks_node_validate($node, $form, &$form_state) {
  if ($node->type == 'group') {
    // See if there already is a global group.
    $group_nid = db_query("SELECT entity_id FROM {field_data_field_group_global} WHERE field_group_global_value = 1")->fetchField();

    // Check if the group that is being created is chosen as global and if there
    // is already an global group.
    if ((is_numeric($group_nid)) && ($node->field_group_global['und'][0]['value'] == 1)) {
      // There is already an global group. Return an error.
      form_set_error('field_group_global', t('There is already a global group in use.'));
    }
  }
}

/**
 * Implements hook_node_insert().
 *
 * Set all new wiki nodes as a new book.
 */
function access_hooks_node_insert($node) {
  if (!isset($_GET['parent'])) {
    if ($node->type == 'wiki') {
      $node->book = array(
        'bid' => 'new',
        'plid' => 0,
      );
    }
  }
}
