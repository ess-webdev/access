<?php
/**
 * @file
 * app_chess_search_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function app_chess_search_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'chess_search_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Chess Search View';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'CHESS Document Search';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_chess_url' => 'field_chess_url',
    'field_chess_fileurl' => 'field_chess_fileurl',
    'title' => 'title',
    'field_chess_description' => 'field_chess_description',
    'field_chess_name' => 'field_chess_name',
    'field_chess_author' => 'field_chess_author',
    'field_chess_modified' => 'field_chess_modified',
    'field_chess_classes' => 'field_chess_classes',
    'field_chess_files' => 'field_chess_files',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_chess_url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_fileurl' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_description' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_author' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_modified' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_classes' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_files' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['header']['area_text_custom']['content'] = 'The CHESS Document Search tool enables you to search for documents published in CHESS without logging in to CHESS.
<br/><br/>
The search result includes only documents that are classified as Published in CHESS. <br/>
Click the <strong>file</strong> link to open a pdf-version of the file or click the <strong>Document Number</strong> to open CHESS.
<br/><br/>
Enter a word or part of word in one or more of the search fields, click Search or press Enter.<br/>
Wildcard searches such as <strong>*</strong> (asterisk) will not work.<br/><br/>';
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'Your search did not match any documents. Please refine your search and try again.';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: url */
  $handler->display->display_options['fields']['field_chess_url']['id'] = 'field_chess_url';
  $handler->display->display_options['fields']['field_chess_url']['table'] = 'field_data_field_chess_url';
  $handler->display->display_options['fields']['field_chess_url']['field'] = 'field_chess_url';
  $handler->display->display_options['fields']['field_chess_url']['label'] = '';
  $handler->display->display_options['fields']['field_chess_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_chess_url']['element_label_colon'] = FALSE;
  /* Field: Content: fileurl */
  $handler->display->display_options['fields']['field_chess_fileurl']['id'] = 'field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['table'] = 'field_data_field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['field'] = 'field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_chess_fileurl']['alter']['make_link'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: description */
  $handler->display->display_options['fields']['field_chess_description']['id'] = 'field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['table'] = 'field_data_field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['field'] = 'field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['label'] = 'Description';
  $handler->display->display_options['fields']['field_chess_description']['element_class'] = '.narrow-table-column';
  /* Field: Content: name */
  $handler->display->display_options['fields']['field_chess_name']['id'] = 'field_chess_name';
  $handler->display->display_options['fields']['field_chess_name']['table'] = 'field_data_field_chess_name';
  $handler->display->display_options['fields']['field_chess_name']['field'] = 'field_chess_name';
  $handler->display->display_options['fields']['field_chess_name']['label'] = 'Document number';
  $handler->display->display_options['fields']['field_chess_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_chess_name']['alter']['path'] = '[field_chess_url]';
  $handler->display->display_options['fields']['field_chess_name']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_chess_name']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_chess_name']['element_label_colon'] = FALSE;
  /* Field: Content: revision */
  $handler->display->display_options['fields']['field_chess_revision']['id'] = 'field_chess_revision';
  $handler->display->display_options['fields']['field_chess_revision']['table'] = 'field_data_field_chess_revision';
  $handler->display->display_options['fields']['field_chess_revision']['field'] = 'field_chess_revision';
  $handler->display->display_options['fields']['field_chess_revision']['label'] = 'Rev.';
  $handler->display->display_options['fields']['field_chess_revision']['element_label_colon'] = FALSE;
  /* Field: Content: author */
  $handler->display->display_options['fields']['field_chess_author']['id'] = 'field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['table'] = 'field_data_field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['field'] = 'field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['label'] = 'Author';
  /* Field: Content: modified */
  $handler->display->display_options['fields']['field_chess_modified']['id'] = 'field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['table'] = 'field_data_field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['field'] = 'field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['label'] = 'Last modified';
  $handler->display->display_options['fields']['field_chess_modified']['settings'] = array(
    'format_type' => 'weather',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Content: classes */
  $handler->display->display_options['fields']['field_chess_classes']['id'] = 'field_chess_classes';
  $handler->display->display_options['fields']['field_chess_classes']['table'] = 'field_data_field_chess_classes';
  $handler->display->display_options['fields']['field_chess_classes']['field'] = 'field_chess_classes';
  $handler->display->display_options['fields']['field_chess_classes']['label'] = 'Classification';
  $handler->display->display_options['fields']['field_chess_classes']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_chess_classes']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_chess_classes']['separator'] = '<br>';
  /* Field: Content: files */
  $handler->display->display_options['fields']['field_chess_files']['id'] = 'field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['table'] = 'field_data_field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['field'] = 'field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['label'] = 'Files';
  $handler->display->display_options['fields']['field_chess_files']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_chess_files']['alter']['path'] = 'chess-search/[field_chess_name]/[field_chess_files-value]/latest';
  $handler->display->display_options['fields']['field_chess_files']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_chess_files']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_chess_files']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_chess_files']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_chess_files']['settings'] = array(
    'trim_length' => '40',
  );
  $handler->display->display_options['fields']['field_chess_files']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_chess_files']['separator'] = '<br/>';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'chess_file' => 'chess_file',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'allwords';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: description (field_chess_description) */
  $handler->display->display_options['filters']['field_chess_description_value']['id'] = 'field_chess_description_value';
  $handler->display->display_options['filters']['field_chess_description_value']['table'] = 'field_data_field_chess_description';
  $handler->display->display_options['filters']['field_chess_description_value']['field'] = 'field_chess_description_value';
  $handler->display->display_options['filters']['field_chess_description_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_chess_description_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_description_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['operator_id'] = 'field_chess_description_value_op';
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['label'] = 'Description';
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['operator'] = 'field_chess_description_value_op';
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['identifier'] = 'field_chess_description_value';
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: name (field_chess_name) */
  $handler->display->display_options['filters']['field_chess_name_value']['id'] = 'field_chess_name_value';
  $handler->display->display_options['filters']['field_chess_name_value']['table'] = 'field_data_field_chess_name';
  $handler->display->display_options['filters']['field_chess_name_value']['field'] = 'field_chess_name_value';
  $handler->display->display_options['filters']['field_chess_name_value']['operator'] = 'allwords';
  $handler->display->display_options['filters']['field_chess_name_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['operator_id'] = 'field_chess_name_value_op';
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['label'] = 'Document number';
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['operator'] = 'field_chess_name_value_op';
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['identifier'] = 'field_chess_name_value';
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: author (field_chess_author) */
  $handler->display->display_options['filters']['field_chess_author_value']['id'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['table'] = 'field_data_field_chess_author';
  $handler->display->display_options['filters']['field_chess_author_value']['field'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['operator'] = 'allwords';
  $handler->display->display_options['filters']['field_chess_author_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_author_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['operator_id'] = 'field_chess_author_value_op';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['label'] = 'Author';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['operator'] = 'field_chess_author_value_op';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['identifier'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: files (field_chess_files) */
  $handler->display->display_options['filters']['field_chess_files_value']['id'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value']['table'] = 'field_data_field_chess_files';
  $handler->display->display_options['filters']['field_chess_files_value']['field'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_chess_files_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_files_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['operator_id'] = 'field_chess_files_value_op';
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['label'] = 'Filename';
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['operator'] = 'field_chess_files_value_op';
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['identifier'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_chess_files_value']['group_info']['label'] = 'files (field_chess_files)';
  $handler->display->display_options['filters']['field_chess_files_value']['group_info']['identifier'] = 'field_chess_files_value';
  $handler->display->display_options['filters']['field_chess_files_value']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['field_chess_files_value']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'chess-search';
  $handler->display->display_options['menu']['title'] = 'chess_view';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $export['chess_search_view'] = $view;

  return $export;
}
