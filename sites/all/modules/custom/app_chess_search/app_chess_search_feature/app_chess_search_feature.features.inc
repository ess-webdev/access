<?php
/**
 * @file
 * app_chess_search_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function app_chess_search_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function app_chess_search_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function app_chess_search_feature_node_info() {
  $items = array(
    'chess_file' => array(
      'name' => t('chess_file'),
      'base' => 'node_content',
      'description' => t('Stores information such as url, title etc. about CHESS files'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
