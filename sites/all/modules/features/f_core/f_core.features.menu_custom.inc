<?php
/**
 * @file
 * f_core.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function f_core_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-menu-taskbar.
  $menus['menu-menu-taskbar'] = array(
    'menu_name' => 'menu-menu-taskbar',
    'title' => 'Taskbar',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Taskbar');


  return $menus;
}
