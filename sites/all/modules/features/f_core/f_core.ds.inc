<?php
/**
 * @file
 * f_core.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function f_core_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'teaser_publishing_info';
  $ds_field->label = 'Teaser publishing info';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<p><a class="author" href="[node:author:url]">[node:author]</a> posted on&nbsp;<span class="created">[node:created:rich_snippets_published_date]</span></p>
',
      'format' => 'full_html',
    ),
    'use_token' => 1,
  );
  $export['teaser_publishing_info'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'teaser_title';
  $ds_field->label = 'Teaser title';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<h3><a href="/node/[node:nid]">[node:title]</a> <span class="type">[node:type]</span></h3>',
      'format' => 'php_code',
    ),
    'use_token' => 1,
  );
  $export['teaser_title'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function f_core_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'small_teaser';
  $ds_view_mode->label = 'Small teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['small_teaser'] = $ds_view_mode;

  return $export;
}
