<?php
/**
 * @file
 * f_core.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function f_core_default_rules_configuration() {
  $items = array();
  $items['rules_redirect_anonymous_users_to_login_page'] = entity_import('rules_config', '{ "rules_redirect_anonymous_users_to_login_page" : {
      "LABEL" : "Redirect anonymous users to login page",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "php" ],
      "ON" : { "init" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        },
        { "php_eval" : { "code" : "$non_redirect = array(\\r\\n  \\u0027user\\u0027,\\r\\n  \\u0027home\\u0027,\\r\\n);\\r\\n\\r\\nif (!in_array(arg(0), $non_redirect)) {\\r\\nreturn TRUE;\\r\\n}\\r\\n" } }
      ],
      "DO" : [
        { "php_eval" : { "code" : "drupal_goto(\\u0027home\\u0027, array(\\u0027query\\u0027 =\\u003E drupal_get_destination()));" } }
      ]
    }
  }');
  $items['rules_redirect_to_node_on_edit_save'] = entity_import('rules_config', '{ "rules_redirect_to_node_on_edit_save" : {
      "LABEL" : "redirect to node on edit save",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : {
                "book" : "book",
                "document" : "document",
                "event" : "event",
                "news" : "news",
                "post" : "post",
                "relevant_articles" : "relevant_articles",
                "wiki" : "wiki"
              }
            }
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "node\\/[node:nid]" } } ]
    }
  }');
  return $items;
}
