<?php
/**
 * @file
 * access_advanced_content_management.features.inc
 */

/**
 * Implements hook_views_api().
 */
function access_advanced_content_management_views_api() {
  return array("api" => "3.0");
}
