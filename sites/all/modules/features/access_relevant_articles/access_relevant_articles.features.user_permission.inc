<?php
/**
 * @file
 * access_relevant_articles.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function access_relevant_articles_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create relevant_articles content'.
  $permissions['create relevant_articles content'] = array(
    'name' => 'create relevant_articles content',
    'roles' => array(
      'content moderator' => 'content moderator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any relevant_articles content'.
  $permissions['delete any relevant_articles content'] = array(
    'name' => 'delete any relevant_articles content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own relevant_articles content'.
  $permissions['delete own relevant_articles content'] = array(
    'name' => 'delete own relevant_articles content',
    'roles' => array(
      'content moderator' => 'content moderator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any relevant_articles content'.
  $permissions['edit any relevant_articles content'] = array(
    'name' => 'edit any relevant_articles content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own relevant_articles content'.
  $permissions['edit own relevant_articles content'] = array(
    'name' => 'edit own relevant_articles content',
    'roles' => array(
      'content moderator' => 'content moderator',
    ),
    'module' => 'node',
  );

  return $permissions;
}
