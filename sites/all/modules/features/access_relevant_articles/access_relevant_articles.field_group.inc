<?php
/**
 * @file
 * access_relevant_articles.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function access_relevant_articles_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_fields|node|relevant_articles|form';
  $field_group->group_name = 'group_main_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'relevant_articles';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group main fields',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Group main fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-main-fields field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_main_fields|node|relevant_articles|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_images|node|relevant_articles|form';
  $field_group->group_name = 'group_post_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'relevant_articles';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group post images',
    'weight' => '1',
    'children' => array(
      0 => 'field_relevant_article_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Group post images',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-post-images field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_post_images|node|relevant_articles|form'] = $field_group;

  return $export;
}
