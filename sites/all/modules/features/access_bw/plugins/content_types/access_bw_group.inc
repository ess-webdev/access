<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('access Group Browsing widget'),
  'description' => t('access Group Browsing widget.'),
  'category' => t('access'),
  'single' => TRUE,
  'content type' => 'access_bw_group',
  'all contexts' => TRUE,
);

/**
* Output function for the '[content_type]' content type.
*/
function access_bw_access_bw_group_content_type_render($subtype, $conf, $panel_args, $context) {
  $context = array_shift($context);
  $node = $context->data;

  $block = new stdClass();
  $block->title = '';
  $widget = access_bw_generate_group_widget($node);
  $block->content = drupal_render($widget['content']);
  return $block;
}

/**
* Returns an edit form for the custom type.
*/
function access_bw_access_bw_group_content_type_edit_form($form, $form_state) {
  return $form;
}

/**
* Returns an edit form for the custom type.
*/
function access_bw_access_bw_group_content_type_edit_form_submit($form, $form_state) {
  return $form;
}
