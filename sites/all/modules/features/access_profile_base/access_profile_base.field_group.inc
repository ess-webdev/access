<?php
/**
 * @file
 * access_profile_base.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function access_profile_base_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_account_options|user|user|form';
  $field_group->group_name = 'group_account_options';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Account options',
    'weight' => '5',
    'children' => array(
      0 => 'account',
      1 => 'timezone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-account-options field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_account_options|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_top|user|user|form';
  $field_group->group_name = 'group_top';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Personal info',
    'weight' => '3',
    'children' => array(
      0 => 'field_name_first',
      1 => 'field_name_last',
      2 => 'field_user_mobile',
      3 => 'field_user_title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Personal info',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-top field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_top|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_work_position|user|user|form';
  $field_group->group_name = 'group_work_position';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Work position',
    'weight' => '4',
    'children' => array(
      0 => 'field_bio',
      1 => 'field_user_division',
      2 => 'field_user_manager',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Work position',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-work-position field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_work_position|user|user|form'] = $field_group;

  return $export;
}
