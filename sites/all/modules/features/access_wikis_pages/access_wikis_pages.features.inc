<?php
/**
 * @file
 * access_wikis_pages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function access_wikis_pages_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function access_wikis_pages_views_api() {
  return array("api" => "3.0");
}
