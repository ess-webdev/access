<?php
/**
 * @file
 * f_vanilla.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function f_vanilla_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function f_vanilla_node_info() {
  $items = array(
    'vanilla' => array(
      'name' => t('Vanilla'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
