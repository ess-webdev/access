<?php
/**
 * @file
 * access_wikis.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function access_wikis_taxonomy_default_vocabularies() {
  return array(
    'employee_actions' => array(
      'name' => 'Employee actions',
      'machine_name' => 'employee_actions',
      'description' => 'Content related to a specific employee action',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
