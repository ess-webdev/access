<?php
/**
 * @file
 * access_profile_social.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function access_profile_social_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_social_links|user|user|form';
  $field_group->group_name = 'group_social_links';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Social links',
    'weight' => '6',
    'children' => array(
      0 => 'field_facebook_url',
      1 => 'field_linkedin_url',
      2 => 'field_twitter_url',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Social links',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-social-links field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_social_links|user|user|form'] = $field_group;

  return $export;
}
