<?php
/**
 * @file
 * access_polls.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function access_polls_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|poll|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'poll';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'group_main_fields',
        1 => 'title',
        2 => 'body',
        3 => 'choice_wrapper',
        4 => 'redirect',
        5 => 'path',
        6 => 'metatags',
      ),
      'right' => array(
        7 => 'field_topics',
        8 => 'og_group_ref',
        9 => 'group_content_access',
        10 => 'settings',
        11 => 'field_radioactivity',
      ),
      'hidden' => array(
        12 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'group_main_fields' => 'left',
      'title' => 'left',
      'body' => 'left',
      'choice_wrapper' => 'left',
      'redirect' => 'left',
      'path' => 'left',
      'metatags' => 'left',
      'field_topics' => 'right',
      'og_group_ref' => 'right',
      'group_content_access' => 'right',
      'settings' => 'right',
      'field_radioactivity' => 'right',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|poll|form'] = $ds_layout;

  return $export;
}
