<?php
/**
 * @file
 * access_polls.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function access_polls_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_fields|node|poll|form';
  $field_group->group_name = 'group_main_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'poll';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group main fields',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Group main fields',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-main-fields field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_main_fields|node|poll|form'] = $field_group;

  return $export;
}
