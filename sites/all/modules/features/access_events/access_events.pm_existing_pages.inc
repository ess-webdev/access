<?php
/**
 * @file
 * access_events.pm_existing_pages.inc
 */

/**
 * Implements hook_pm_existing_pages_info().
 */
function access_events_pm_existing_pages_info() {
  $export = array();

  $pm_existing_page = new stdClass();
  $pm_existing_page->api_version = 1;
  $pm_existing_page->name = 'calendar_day';
  $pm_existing_page->label = 'Calendar day';
  $pm_existing_page->context = '';
  $pm_existing_page->paths = 'calendar/day';
  $export['calendar_day'] = $pm_existing_page;

  $pm_existing_page = new stdClass();
  $pm_existing_page->api_version = 1;
  $pm_existing_page->name = 'calendar_month';
  $pm_existing_page->label = 'Calendar month';
  $pm_existing_page->context = '';
  $pm_existing_page->paths = 'calendar/month';
  $export['calendar_month'] = $pm_existing_page;

  return $export;
}
