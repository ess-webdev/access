<?php
/**
 * @file
 * glossary.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function glossary_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'app_glossary';
  $view->description = 'New glossary';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Glossary';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Glossary';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_glossary_term_abbreviation' => 'field_glossary_term_abbreviation',
    'field_glossary_term_full_term' => 'field_glossary_term_full_term',
    'field_glossary_term_description' => 'field_glossary_term_description',
    'field_glossary_term_context' => 'field_glossary_term_context',
    'changed' => 'changed',
  );
  $handler->display->display_options['style_options']['default'] = 'field_glossary_term_full_term';
  $handler->display->display_options['style_options']['info'] = array(
    'field_glossary_term_abbreviation' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_glossary_term_full_term' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_glossary_term_description' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_glossary_term_context' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<div class="glossary-intro">The ESS is a complex organisation, involving highly specific scientific, technical and administrative activities, and involving a large number of stakeholders of geographical, organisational and professional diversity. In addition, the activities within ESS are dependent on a large number of specific terms, abbreviations and proper names.

Clarity and consistency in the usage of these terms and words are crucial for the efficient pursuit of the ESS goals. The ESS Glossary is set up with the aim of uniform usage of terms, abbreviations and proper names.

The ESS Glossary shall list all terms, acronyms, abbreviations and proper names that appear or can appear in documents and communication produced within the ESS, which also
<ul>
<li>are of major use within ESS</li>
<li>need to be explained or described</li>
<li>there is a need to standardise the usage of the term in question.</li>
</ul>

The ESS Glossary specifies the correct terms and defines the correct spelling for words describing scientific, technical, administrative etc objects, equipment, processes, bodies, phenomena etc used within ESS.

Spelling and usage specified in the ESS Glossary shall be used by all ESS staff, suppliers and within all ESS collaborations.

New items for the glossary can be sent to <a href="mailto:glossary@esss.se">glossary@esss.se</a>. The suggestions will be reviewed and approved by a cross-functional group on a quarterly basis.</div>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Header: Global: PHP */
  $handler->display->display_options['header']['php']['id'] = 'php';
  $handler->display->display_options['header']['php']['table'] = 'views';
  $handler->display->display_options['header']['php']['field'] = 'php';
  $handler->display->display_options['header']['php']['empty'] = TRUE;
  $handler->display->display_options['header']['php']['php_output'] = '<?php
$url = $_SERVER[\'REQUEST_URI\'];
if( strrpos($url, \'?\') ) $url = substr($url, 0, strrpos($url, \'?\'));
if( strlen($url) - strrpos($url, \'/\') < 3) {
$url = substr($url, 0, strrpos($url, \'/\'));
}
echo "<div class=\\"glossary-letters\\">";
echo "<div class=\\"glossary-letter all\\"><a href=\\"$url\\">All</a></div>";
for($i = 65; $i <= 90; $i++) {
$chr = chr($i);
echo("<div class=\\"glossary-letter\\"><a href=\\"$url/$chr\\">$chr</a></div>");
//if($i < 89) { echo(" | "); }
}
echo "</div>";
?>';
  /* Field: Content: Abbreviation */
  $handler->display->display_options['fields']['field_glossary_term_abbreviation']['id'] = 'field_glossary_term_abbreviation';
  $handler->display->display_options['fields']['field_glossary_term_abbreviation']['table'] = 'field_data_field_glossary_term_abbreviation';
  $handler->display->display_options['fields']['field_glossary_term_abbreviation']['field'] = 'field_glossary_term_abbreviation';
  $handler->display->display_options['fields']['field_glossary_term_abbreviation']['element_label_colon'] = FALSE;
  /* Field: Content: Term */
  $handler->display->display_options['fields']['field_glossary_term_full_term']['id'] = 'field_glossary_term_full_term';
  $handler->display->display_options['fields']['field_glossary_term_full_term']['table'] = 'field_data_field_glossary_term_full_term';
  $handler->display->display_options['fields']['field_glossary_term_full_term']['field'] = 'field_glossary_term_full_term';
  $handler->display->display_options['fields']['field_glossary_term_full_term']['element_label_colon'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_glossary_term_description']['id'] = 'field_glossary_term_description';
  $handler->display->display_options['fields']['field_glossary_term_description']['table'] = 'field_data_field_glossary_term_description';
  $handler->display->display_options['fields']['field_glossary_term_description']['field'] = 'field_glossary_term_description';
  $handler->display->display_options['fields']['field_glossary_term_description']['element_label_colon'] = FALSE;
  /* Field: Content: Context */
  $handler->display->display_options['fields']['field_glossary_term_context']['id'] = 'field_glossary_term_context';
  $handler->display->display_options['fields']['field_glossary_term_context']['table'] = 'field_data_field_glossary_term_context';
  $handler->display->display_options['fields']['field_glossary_term_context']['field'] = 'field_glossary_term_context';
  $handler->display->display_options['fields']['field_glossary_term_context']['element_label_colon'] = FALSE;
  /* Field: Content: Updated date */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'node';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Last updated';
  $handler->display->display_options['fields']['changed']['alter']['text'] = '<div style="width: 100px;">[changed]</div>';
  $handler->display->display_options['fields']['changed']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['changed']['date_format'] = 'short';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['order'] = 'DESC';
  /* Contextual filter: Content: Abbreviation (field_glossary_term_abbreviation) */
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['id'] = 'field_glossary_term_abbreviation_value';
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['table'] = 'field_data_field_glossary_term_abbreviation';
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['field'] = 'field_glossary_term_abbreviation_value';
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['glossary'] = TRUE;
  $handler->display->display_options['arguments']['field_glossary_term_abbreviation_value']['limit'] = '1';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'glossary_term' => 'glossary_term',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Abbreviation (field_glossary_term_abbreviation) */
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['id'] = 'field_glossary_term_abbreviation_value';
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['table'] = 'field_data_field_glossary_term_abbreviation';
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['field'] = 'field_glossary_term_abbreviation_value';
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['group'] = 1;
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['expose']['operator_id'] = 'field_glossary_term_abbreviation_value_op';
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['expose']['label'] = 'Abbreviation';
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['expose']['operator'] = 'field_glossary_term_abbreviation_value_op';
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['expose']['identifier'] = 'field_glossary_term_abbreviation_value_1';
  $handler->display->display_options['filters']['field_glossary_term_abbreviation_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    8 => 0,
    38 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
  );
  /* Filter criterion: Content: Term (field_glossary_term_full_term) */
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['id'] = 'field_glossary_term_full_term_value';
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['table'] = 'field_data_field_glossary_term_full_term';
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['field'] = 'field_glossary_term_full_term_value';
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['group'] = 1;
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['expose']['operator_id'] = 'field_glossary_term_full_term_value_op';
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['expose']['label'] = 'Term';
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['expose']['operator'] = 'field_glossary_term_full_term_value_op';
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['expose']['identifier'] = 'field_glossary_term_full_term_value';
  $handler->display->display_options['filters']['field_glossary_term_full_term_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    8 => 0,
    38 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
  );
  /* Filter criterion: Content: Description (field_glossary_term_description) */
  $handler->display->display_options['filters']['field_glossary_term_description_value']['id'] = 'field_glossary_term_description_value';
  $handler->display->display_options['filters']['field_glossary_term_description_value']['table'] = 'field_data_field_glossary_term_description';
  $handler->display->display_options['filters']['field_glossary_term_description_value']['field'] = 'field_glossary_term_description_value';
  $handler->display->display_options['filters']['field_glossary_term_description_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_glossary_term_description_value']['group'] = 1;
  $handler->display->display_options['filters']['field_glossary_term_description_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_glossary_term_description_value']['expose']['operator_id'] = 'field_glossary_term_description_value_op';
  $handler->display->display_options['filters']['field_glossary_term_description_value']['expose']['label'] = 'Description';
  $handler->display->display_options['filters']['field_glossary_term_description_value']['expose']['operator'] = 'field_glossary_term_description_value_op';
  $handler->display->display_options['filters']['field_glossary_term_description_value']['expose']['identifier'] = 'field_glossary_term_description_value';
  $handler->display->display_options['filters']['field_glossary_term_description_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    8 => 0,
    38 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
  );
  /* Filter criterion: Content: Context (field_glossary_term_context) */
  $handler->display->display_options['filters']['field_glossary_term_context_value']['id'] = 'field_glossary_term_context_value';
  $handler->display->display_options['filters']['field_glossary_term_context_value']['table'] = 'field_data_field_glossary_term_context';
  $handler->display->display_options['filters']['field_glossary_term_context_value']['field'] = 'field_glossary_term_context_value';
  $handler->display->display_options['filters']['field_glossary_term_context_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_glossary_term_context_value']['group'] = 1;
  $handler->display->display_options['filters']['field_glossary_term_context_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_glossary_term_context_value']['expose']['operator_id'] = 'field_glossary_term_context_value_op';
  $handler->display->display_options['filters']['field_glossary_term_context_value']['expose']['label'] = 'Context';
  $handler->display->display_options['filters']['field_glossary_term_context_value']['expose']['operator'] = 'field_glossary_term_context_value_op';
  $handler->display->display_options['filters']['field_glossary_term_context_value']['expose']['identifier'] = 'field_glossary_term_context_value';
  $handler->display->display_options['filters']['field_glossary_term_context_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    8 => 0,
    38 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'glossary';
  $export['app_glossary'] = $view;

  $view = new view();
  $view->name = 'app_glossary_export';
  $view->description = 'Exports glossary terms to a .csv file';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'app_glossary_export';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'import glossary_term_importer feeds';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Abbreviation */
  $handler->display->display_options['fields']['field_glossary_term_abbreviation']['id'] = 'field_glossary_term_abbreviation';
  $handler->display->display_options['fields']['field_glossary_term_abbreviation']['table'] = 'field_data_field_glossary_term_abbreviation';
  $handler->display->display_options['fields']['field_glossary_term_abbreviation']['field'] = 'field_glossary_term_abbreviation';
  $handler->display->display_options['fields']['field_glossary_term_abbreviation']['element_label_colon'] = FALSE;
  /* Field: Content: Term */
  $handler->display->display_options['fields']['field_glossary_term_full_term']['id'] = 'field_glossary_term_full_term';
  $handler->display->display_options['fields']['field_glossary_term_full_term']['table'] = 'field_data_field_glossary_term_full_term';
  $handler->display->display_options['fields']['field_glossary_term_full_term']['field'] = 'field_glossary_term_full_term';
  $handler->display->display_options['fields']['field_glossary_term_full_term']['element_label_colon'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_glossary_term_description']['id'] = 'field_glossary_term_description';
  $handler->display->display_options['fields']['field_glossary_term_description']['table'] = 'field_data_field_glossary_term_description';
  $handler->display->display_options['fields']['field_glossary_term_description']['field'] = 'field_glossary_term_description';
  $handler->display->display_options['fields']['field_glossary_term_description']['element_label_colon'] = FALSE;
  /* Field: Content: Context */
  $handler->display->display_options['fields']['field_glossary_term_context']['id'] = 'field_glossary_term_context';
  $handler->display->display_options['fields']['field_glossary_term_context']['table'] = 'field_data_field_glossary_term_context';
  $handler->display->display_options['fields']['field_glossary_term_context']['field'] = 'field_glossary_term_context';
  $handler->display->display_options['fields']['field_glossary_term_context']['element_label_colon'] = FALSE;
  /* Field: Content: Term owner (hidden) */
  $handler->display->display_options['fields']['field_glossary_term_owner']['id'] = 'field_glossary_term_owner';
  $handler->display->display_options['fields']['field_glossary_term_owner']['table'] = 'field_data_field_glossary_term_owner';
  $handler->display->display_options['fields']['field_glossary_term_owner']['field'] = 'field_glossary_term_owner';
  $handler->display->display_options['fields']['field_glossary_term_owner']['label'] = 'Owner';
  $handler->display->display_options['fields']['field_glossary_term_owner']['element_label_colon'] = FALSE;
  /* Field: Content: Comment (hidden) */
  $handler->display->display_options['fields']['field_glossary_term_comment']['id'] = 'field_glossary_term_comment';
  $handler->display->display_options['fields']['field_glossary_term_comment']['table'] = 'field_data_field_glossary_term_comment';
  $handler->display->display_options['fields']['field_glossary_term_comment']['field'] = 'field_glossary_term_comment';
  $handler->display->display_options['fields']['field_glossary_term_comment']['label'] = 'Comment';
  $handler->display->display_options['fields']['field_glossary_term_comment']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Term (field_glossary_term_full_term) */
  $handler->display->display_options['sorts']['field_glossary_term_full_term_value']['id'] = 'field_glossary_term_full_term_value';
  $handler->display->display_options['sorts']['field_glossary_term_full_term_value']['table'] = 'field_data_field_glossary_term_full_term';
  $handler->display->display_options['sorts']['field_glossary_term_full_term_value']['field'] = 'field_glossary_term_full_term_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'glossary_term' => 'glossary_term',
  );

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'glossary-export';
  $export['app_glossary_export'] = $view;

  return $export;
}
