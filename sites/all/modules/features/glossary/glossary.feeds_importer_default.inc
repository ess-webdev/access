<?php
/**
 * @file
 * glossary.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function glossary_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'glossary_term_importer';
  $feeds_importer->config = array(
    'name' => 'Glossary Term Importer',
    'description' => 'Imports a CSV with glossary terms',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'abbreviation',
            'target' => 'field_glossary_term_abbreviation',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'term',
            'target' => 'title',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'term',
            'target' => 'field_glossary_term_full_term',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'description',
            'target' => 'field_glossary_term_description',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'context',
            'target' => 'field_glossary_term_context',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'owner',
            'target' => 'field_glossary_term_owner:name',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'comment',
            'target' => 'field_glossary_term_comment',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'glossary_term',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['glossary_term_importer'] = $feeds_importer;

  return $export;
}
