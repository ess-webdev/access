<?php
/**
 * @file
 * access_like.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function access_like_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function access_like_flag_default_flags() {
  $flags = array();
  // Exported flag: "General content like flag".
  $flags['access_content_like'] = array(
    'content_type' => 'node',
    'title' => 'General content like flag',
    'global' => 0,
    'types' => array(
      0 => 'document',
      1 => 'event',
      2 => 'relevant_articles',
      3 => 'news',
      4 => 'post',
      5 => 'wiki',
    ),
    'flag_short' => 'Like this [node:content-type]',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Un-like this [node:content-type]',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
        4 => 6,
        5 => 7,
        6 => 8,
      ),
      'unflag' => array(
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
        4 => 6,
        5 => 7,
        6 => 8,
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'access_like',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;

}

/**
 * Implements hook_default_message_type().
 */
function access_like_default_message_type() {
  $items = array();
  $items['access_like_user_likes_node'] = entity_import('message_type', '{
    "name" : "access_like_user_likes_node",
    "description" : "access like: User likes a node",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : { "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" } },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:user:picture]",
          "format" : "full_html",
          "safe_value" : " \\u003Cp\\u003E[message:user:picture]\\u003C\\/p\\u003E\\n "
        },
        {
          "value" : "\\u003Ca href=\\u0022[message:user:url:absolute]\\u0022\\u003E[message:user:name]\\u003C\\/a\\u003E liked the [message:field-target-nodes:0:type] \\u003Ca href=\\u0022[message:field-target-nodes:0:url]\\u0022\\u003E[message:field-target-nodes:0:title]\\u003C\\/a\\u003E",
          "format" : "full_html",
          "safe_value" : " \\u003Cp\\u003E\\u003Ca href=\\u0022[message:user:url:absolute]\\u0022\\u003E[message:user:name]\\u003C\\/a\\u003E liked the [message:field-target-nodes:0:type] \\u003Ca href=\\u0022[message:field-target-nodes:0:url]\\u0022\\u003E[message:field-target-nodes:0:title]\\u003C\\/a\\u003E\\u003C\\/p\\u003E\\n "
        }
      ]
    },
    "rdf_mapping" : []
  }');
  return $items;
}
