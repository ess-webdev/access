<?php
/**
 * @file
 * access_like.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function access_like_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'content_like_count';
  $ds_field->label = 'Content like count';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?php
$nid = arg(1);
$flag = flag_get_flag(\'access_content_like\') or die(\'no flag\');
print "Current: ";
print \'<span class="count">\' . $flag->get_count($nid) . \'</span>\';
?>',
      'format' => 'php_code',
    ),
    'use_token' => 0,
  );
  $export['content_like_count'] = $ds_field;

  return $export;
}
