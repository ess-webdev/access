<?php
/**
 * @file
 * access_development_banner.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function access_development_banner_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'dev_banner_enabled';
  $strongarm->value = 1;
  $export['dev_banner_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'dev_banner_image_set';
  $strongarm->value = '1';
  $export['dev_banner_image_set'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'dev_banner_position';
  $strongarm->value = 'ne';
  $export['dev_banner_position'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'dev_banner_sticky';
  $strongarm->value = 1;
  $export['dev_banner_sticky'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'dev_banner_url_devel';
  $strongarm->value = 'access.local:8888';
  $export['dev_banner_url_devel'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'dev_banner_url_stage';
  $strongarm->value = '';
  $export['dev_banner_url_stage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'dev_banner_url_test';
  $strongarm->value = 'access.webdev.esss.lu.se';
  $export['dev_banner_url_test'] = $strongarm;

  return $export;
}
