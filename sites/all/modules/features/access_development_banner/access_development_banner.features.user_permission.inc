<?php
/**
 * @file
 * access_development_banner.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function access_development_banner_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer development banner.
  $permissions['administer development banner'] = array(
    'name' => 'administer development banner',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'dev_banner',
  );

  // Exported permission: view development banner.
  $permissions['view development banner'] = array(
    'name' => 'view development banner',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'dev_banner',
  );

  return $permissions;
}
