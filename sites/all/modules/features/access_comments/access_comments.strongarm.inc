<?php
/**
 * @file
 * access_comments.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function access_comments_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'simplify_comments_global';
  $strongarm->value = array(
    0 => 'format',
  );
  $export['simplify_comments_global'] = $strongarm;

  return $export;
}
