<?php
/**
 * @file
 * Code for the access Posts feature.
 */

include_once 'access_posts.features.inc';

/**
* Implements hook_access_bw_group_widget().
*/
function access_posts_access_bw_group_widget() {
  return array(
    'access_posts' => array(
      'title' => 'Posts',
      'type' => 'view',
      'vid' => 'access_bw_posts',
      'display' => 'default',
      'weight' => -1,
    ),
  );
}

/**
 * Implements hook_access_bw_create_all_widget().
 */
function access_posts_access_bw_create_all_widget($group) {
  if (og_user_access('node', $group->nid, 'create post content')) {
    $link = l(t('Create a post'),'node/add/post',
    array('attributes' => array('class' => 'access-posts-create'), 'query' => array('og_group_ref' => $group->nid))
    );
    return array(
      'access_posts' => array(
        'default' => TRUE,
        'link' => $link,
        'text' =>  t('Foster a topic through commenting'),
        '#weight' => -1,
      ),
    );
  }
}


/**
 * Implements hook_access_entity_integration.
 */
function access_posts_access_entity_integration() {
  return array(
    'node' => array(
      'post' => array(
      ),
    ),
  );
}

/**
* Implements hook_views_pre_render().
*/
function access_posts_views_pre_render(&$view) {
  if ($view->name == 'access_bw_posts') {
    $group_id = $view->args[0];
    if (og_user_access('node', $group_id, 'create post content')) {
      $view->attachment_before = l(t('Create a post'), 'node/add/post', array('query' => array('og_group_ref' => $group_id)));
    }
  }
}

/**
* Implements hook_strongarm_alter().
*/
 function access_posts_strongarm_alter(&$items) {
  // Expose the Post content type for 'liking' via the access_like module
  // by altering the configuration for the Rate.module widget that it provides.
  if (!empty($items['rate_widgets']->value)) {
    foreach($items['rate_widgets']->value as $key => $widget) {
      if ($widget->name == 'access_like') {
        if (!in_array('post', $items['rate_widgets']->value[$key]->node_types)) {
          $items['rate_widgets']->value[$key]->node_types[] = 'post';
        }
        if (!in_array('post', $items['rate_widgets']->value[$key]->comment_types)) {
          $items['rate_widgets']->value[$key]->comment_types[] = 'post';
        }
      }
    }
  }
  // Expose the post content type for integration with access Radioactivity
  // and access Groups.
  foreach (array('access_radioactivity_entity_types', 'access_groups_entity_types') as $key) {
    if (isset($items[$key])) {
      $items[$key]->value['node']['post'] = 1;
    }
  }
}
