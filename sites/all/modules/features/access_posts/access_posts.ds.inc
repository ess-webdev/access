<?php
/**
 * @file
 * access_posts.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function access_posts_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|post|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'post';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
        'ft' => array(),
      ),
    ),
    'author' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'author',
    ),
    'post_date' => array(
      'weight' => '0',
      'label' => 'above',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'On',
        ),
      ),
    ),
    'body' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'description',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
  );
  $export['node|post|search_result'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function access_posts_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|post|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'post';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'body',
        1 => 'field_radioactivity',
        2 => 'field_topics',
        3 => 'group_content_access',
        4 => 'og_group_ref',
        5 => 'field_post_files',
        6 => 'field_post_image',
        7 => 'field_post_employee_actions',
      ),
    ),
    'fields' => array(
      'body' => 'ds_content',
      'field_radioactivity' => 'ds_content',
      'field_topics' => 'ds_content',
      'group_content_access' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_post_files' => 'ds_content',
      'field_post_image' => 'ds_content',
      'field_post_employee_actions' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['node|post|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|post|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'post';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'author',
        2 => 'post_date',
        3 => 'field_radioactivity',
        4 => 'og_group_ref',
        5 => 'group_content_access',
        6 => 'body',
        7 => 'field_topics',
        8 => 'field_post_files',
        9 => 'field_post_image',
        10 => 'field_post_employee_actions',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'author' => 'ds_content',
      'post_date' => 'ds_content',
      'field_radioactivity' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'group_content_access' => 'ds_content',
      'body' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_post_files' => 'ds_content',
      'field_post_image' => 'ds_content',
      'field_post_employee_actions' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|post|search_result'] = $ds_layout;

  return $export;
}
