<?php
/**
 * @file
 * access_follow_node.features.inc
 */

/**
 * Implements hook_views_api().
 */
function access_follow_node_views_api() {
  return array("api" => "3.0");
}
