<?php
/**
 * @file
 * access_follow_term.features.inc
 */

/**
 * Implements hook_views_api().
 */
function access_follow_term_views_api() {
  return array("api" => "3.0");
}
