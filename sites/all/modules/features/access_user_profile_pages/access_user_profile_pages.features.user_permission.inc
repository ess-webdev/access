<?php
/**
 * @file
 * access_user_profile_pages.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function access_user_profile_pages_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'edit own user account'.
  $permissions['edit own user account'] = array(
    'name' => 'edit own user account',
    'roles' => array(
      'ESSstaff' => 'ESSstaff',
    ),
    'module' => 'edit_own_user_account_permission',
  );

  return $permissions;
}
