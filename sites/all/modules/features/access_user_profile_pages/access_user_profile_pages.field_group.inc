<?php
/**
 * @file
 * access_user_profile_pages.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function access_user_profile_pages_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info|user|user|full';
  $field_group->group_name = 'group_info';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group info',
    'weight' => '4',
    'children' => array(
      0 => 'field_user_division',
      1 => 'field_user_manager',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Group info',
      'instance_settings' => array(
        'classes' => 'group-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_info|user|user|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_fields|user|user|form';
  $field_group->group_name = 'group_main_fields';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group main fields',
    'weight' => '18',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Group main fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-main-fields',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_main_fields|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_right_top|user|user|full';
  $field_group->group_name = 'group_right_top';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Right top',
    'weight' => '2',
    'children' => array(
      0 => 'field_user_mobile',
      1 => 'field_user_title',
      2 => 'user_email',
      3 => 'name',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Right top',
      'instance_settings' => array(
        'classes' => 'group-right-top field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_right_top|user|user|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sidebar_info|user|user|full';
  $field_group->group_name = 'group_sidebar_info';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sidebar info',
    'weight' => '1',
    'children' => array(
      0 => 'message_subscribe_email',
      1 => 'og_user_node',
      2 => 'summary',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Sidebar info',
      'instance_settings' => array(
        'classes' => 'sidebar-info',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_sidebar_info|user|user|full'] = $field_group;

  return $export;
}
