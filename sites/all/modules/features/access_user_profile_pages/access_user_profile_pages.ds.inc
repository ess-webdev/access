<?php
/**
 * @file
 * access_user_profile_pages.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function access_user_profile_pages_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'user|user|full';
  $ds_fieldsetting->entity_type = 'user';
  $ds_fieldsetting->bundle = 'user';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'activity_feed_block' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'activity-feed',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'user_email' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
        ),
      ),
    ),
    'name' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h2',
        'class' => 'user-name',
        'ft' => array(
          'func' => 'theme_ds_field_expert',
        ),
      ),
    ),
    'ds_user_picture' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'ds_picture_user-picture',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'user-picture',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_user_mobile' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'fi' => TRUE,
          'fi-el' => 'span',
          'fi-cl' => 'mobile',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
    'field_user_title' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'fi' => TRUE,
          'fi-el' => 'h3',
          'fi-cl' => 'field-name-field-user-title',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
  );
  $export['user|user|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function access_user_profile_pages_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'activity_feed';
  $ds_field->label = 'Activity feed';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|7d4b68fe5850efd34f1d8355d827d19b',
    'block_render' => '1',
  );
  $export['activity_feed'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'activity_feed_block';
  $ds_field->label = 'Activity feed';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'user' => 'user',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|7d4b68fe5850efd34f1d8355d827d19b',
    'block_render' => '1',
  );
  $export['activity_feed_block'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'user_email';
  $ds_field->label = 'User email';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'user' => 'user',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<a class="user-mail" href="mailto:[user:mail]">[user:mail]</a>',
      'format' => 'php_code',
    ),
    'use_token' => 1,
  );
  $export['user_email'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function access_user_profile_pages_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'user|user|default';
  $ds_layout->entity_type = 'user';
  $ds_layout->bundle = 'user';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'field_name_first',
        1 => 'field_name_last',
        2 => 'field_bio',
      ),
      'left' => array(
        3 => 'field_user_mobile',
        4 => 'og_user_node',
        5 => 'og_user_node1',
        6 => 'summary',
        7 => 'message_subscribe_email',
        8 => 'field_user_division',
        9 => 'field_user_manager',
      ),
      'right' => array(
        10 => 'field_linkedin_url',
        11 => 'field_facebook_url',
        12 => 'field_twitter_url',
      ),
    ),
    'fields' => array(
      'field_name_first' => 'header',
      'field_name_last' => 'header',
      'field_bio' => 'header',
      'field_user_mobile' => 'left',
      'og_user_node' => 'left',
      'og_user_node1' => 'left',
      'summary' => 'left',
      'message_subscribe_email' => 'left',
      'field_user_division' => 'left',
      'field_user_manager' => 'left',
      'field_linkedin_url' => 'right',
      'field_facebook_url' => 'right',
      'field_twitter_url' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['user|user|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'user|user|form';
  $ds_layout->entity_type = 'user';
  $ds_layout->bundle = 'user';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_3col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'picture',
        1 => 'og_user_node1',
        2 => 'message_subscribe_email',
      ),
      'middle' => array(
        3 => 'group_top',
        4 => 'group_work_position',
        5 => 'field_name_first',
        6 => 'group_account_options',
        7 => 'field_user_division',
        8 => 'field_name_last',
        10 => 'field_user_manager',
        11 => 'field_user_mobile',
        12 => 'field_user_title',
        13 => 'field_bio',
        19 => 'account',
        20 => 'timezone',
      ),
      'right' => array(
        9 => 'group_social_links',
        14 => 'field_facebook_url',
        15 => 'field_linkedin_url',
        16 => 'field_twitter_url',
      ),
      'hidden' => array(
        17 => 'og_user_node',
        18 => 'ldap_user_puid_sid',
        21 => 'ldap_user_puid',
        22 => 'ldap_user_puid_property',
        23 => 'ldap_user_current_dn',
        24 => 'ldap_user_prov_entries',
        25 => 'ldap_user_last_checked',
        26 => 'ldap_user_ldap_exclude',
        27 => 'group_main_fields',
        28 => 'redirect',
        29 => 'metatags',
        30 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'picture' => 'left',
      'og_user_node1' => 'left',
      'message_subscribe_email' => 'left',
      'group_top' => 'middle',
      'group_work_position' => 'middle',
      'field_name_first' => 'middle',
      'group_account_options' => 'middle',
      'field_user_division' => 'middle',
      'field_name_last' => 'middle',
      'group_social_links' => 'right',
      'field_user_manager' => 'middle',
      'field_user_mobile' => 'middle',
      'field_user_title' => 'middle',
      'field_bio' => 'middle',
      'field_facebook_url' => 'right',
      'field_linkedin_url' => 'right',
      'field_twitter_url' => 'right',
      'og_user_node' => 'hidden',
      'ldap_user_puid_sid' => 'hidden',
      'account' => 'middle',
      'timezone' => 'middle',
      'ldap_user_puid' => 'hidden',
      'ldap_user_puid_property' => 'hidden',
      'ldap_user_current_dn' => 'hidden',
      'ldap_user_prov_entries' => 'hidden',
      'ldap_user_last_checked' => 'hidden',
      'ldap_user_ldap_exclude' => 'hidden',
      'group_main_fields' => 'hidden',
      'redirect' => 'hidden',
      'metatags' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['user|user|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'user|user|full';
  $ds_layout->entity_type = 'user';
  $ds_layout->bundle = 'user';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'ds_user_picture',
        1 => 'group_sidebar_info',
        2 => 'summary',
        6 => 'message_subscribe_email',
        8 => 'og_user_node',
      ),
      'right' => array(
        3 => 'group_right_top',
        4 => 'name',
        5 => 'field_bio',
        7 => 'group_info',
        9 => 'field_user_title',
        10 => 'user_email',
        11 => 'activity_feed_block',
        12 => 'field_user_division',
        13 => 'field_user_mobile',
        14 => 'field_user_manager',
      ),
    ),
    'fields' => array(
      'ds_user_picture' => 'left',
      'group_sidebar_info' => 'left',
      'summary' => 'left',
      'group_right_top' => 'right',
      'name' => 'right',
      'field_bio' => 'right',
      'message_subscribe_email' => 'left',
      'group_info' => 'right',
      'og_user_node' => 'left',
      'field_user_title' => 'right',
      'user_email' => 'right',
      'activity_feed_block' => 'right',
      'field_user_division' => 'right',
      'field_user_mobile' => 'right',
      'field_user_manager' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['user|user|full'] = $ds_layout;

  return $export;
}
