<?php
/**
 * @file
 * access_user_profile_pages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function access_user_profile_pages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function access_user_profile_pages_image_default_styles() {
  $styles = array();

  // Exported image style: user-picture.
  $styles['user-picture'] = array(
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 235,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
