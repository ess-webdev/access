<?php
/**
 * @file
 * f_post.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function f_post_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_fields|node|post|form';
  $field_group->group_name = 'group_main_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Main fields',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Main fields',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-main-fields field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_main_fields|node|post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_files|node|post|form';
  $field_group->group_name = 'group_post_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group post files',
    'weight' => '3',
    'children' => array(
      0 => 'field_post_files',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Group post files',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-post-files field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_post_files|node|post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_images|node|post|form';
  $field_group->group_name = 'group_post_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group post images',
    'weight' => '2',
    'children' => array(
      0 => 'field_post_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Group post images',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-post-images field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_post_images|node|post|form'] = $field_group;

  return $export;
}
