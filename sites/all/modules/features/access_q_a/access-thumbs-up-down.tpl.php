<?php
/**
 * @file
 * Rate widget theme
 */
?>

<div class="access-q-a-rate-up-down clearfix">
  <?php
  print '<div class="rate-info"><span class="rate-info-value">' . $results['rating'] . '</span> <span class="rate-info-label">' . format_plural($results['rating'], 'point', 'points') . '</span></div>';

  if ($display_options['description']) {
    print '<div class="rate-description">' . $display_options['description'] . '</div>';
  }
  ?>

  <div class="access-q-a-rate-buttons">
    <div class="access-q-a-rate-trigger access-q-a-rate-up">
      <?php print $up_button; ?>
    </div>
    <div class="access-q-a-rate-trigger access-q-a-rate-down">
      <?php print $down_button; ?>
    </div>
  </div>
</div>
