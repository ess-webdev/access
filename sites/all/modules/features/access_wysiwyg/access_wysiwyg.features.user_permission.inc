<?php
/**
 * @file
 * access_wysiwyg.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function access_wysiwyg_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use text format filtered_html'.
  $permissions['use text format filtered_html'] = array(
    'name' => 'use text format filtered_html',
    'roles' => array(
      'ESSstaff' => 'ESSstaff',
      'External' => 'External',
      'administrator' => 'administrator',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
