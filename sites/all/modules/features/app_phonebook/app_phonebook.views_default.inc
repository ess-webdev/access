<?php
/**
 * @file
 * app_phonebook.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function app_phonebook_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'phonebook_users';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'Phonebook';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Phonebook';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '40';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = TRUE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'field_user_telephone' => 'field_user_telephone',
    'field_user_mobile' => 'field_user_mobile',
    'field_user_directorate' => 'field_user_directorate',
    'field_user_division' => 'field_user_division',
    'mail' => 'mail',
  );
  $handler->display->display_options['style_options']['default'] = 'name';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_user_telephone' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_user_mobile' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_user_directorate' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_user_division' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'mail' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: User: Telephone */
  $handler->display->display_options['fields']['field_user_telephone']['id'] = 'field_user_telephone';
  $handler->display->display_options['fields']['field_user_telephone']['table'] = 'field_data_field_user_telephone';
  $handler->display->display_options['fields']['field_user_telephone']['field'] = 'field_user_telephone';
  /* Field: User: Mobile */
  $handler->display->display_options['fields']['field_user_mobile']['id'] = 'field_user_mobile';
  $handler->display->display_options['fields']['field_user_mobile']['table'] = 'field_data_field_user_mobile';
  $handler->display->display_options['fields']['field_user_mobile']['field'] = 'field_user_mobile';
  /* Field: User: Directorate */
  $handler->display->display_options['fields']['field_user_directorate']['id'] = 'field_user_directorate';
  $handler->display->display_options['fields']['field_user_directorate']['table'] = 'field_data_field_user_directorate';
  $handler->display->display_options['fields']['field_user_directorate']['field'] = 'field_user_directorate';
  /* Field: User: Division */
  $handler->display->display_options['fields']['field_user_division']['id'] = 'field_user_division';
  $handler->display->display_options['fields']['field_user_division']['table'] = 'field_data_field_user_division';
  $handler->display->display_options['fields']['field_user_division']['field'] = 'field_user_division';
  /* Field: User: E-mail */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'users';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  /* Sort criterion: User: Created date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'users';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Realname: Name */
  $handler->display->display_options['filters']['realname']['id'] = 'realname';
  $handler->display->display_options['filters']['realname']['table'] = 'realname';
  $handler->display->display_options['filters']['realname']['field'] = 'realname';
  $handler->display->display_options['filters']['realname']['operator'] = 'contains';
  $handler->display->display_options['filters']['realname']['group'] = 1;
  $handler->display->display_options['filters']['realname']['exposed'] = TRUE;
  $handler->display->display_options['filters']['realname']['expose']['operator_id'] = 'realname_op';
  $handler->display->display_options['filters']['realname']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['realname']['expose']['operator'] = 'realname_op';
  $handler->display->display_options['filters']['realname']['expose']['identifier'] = 'realname';
  $handler->display->display_options['filters']['realname']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    8 => 0,
    38 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
  );
  /* Filter criterion: User: Telephone (field_user_telephone) */
  $handler->display->display_options['filters']['field_user_telephone_value']['id'] = 'field_user_telephone_value';
  $handler->display->display_options['filters']['field_user_telephone_value']['table'] = 'field_data_field_user_telephone';
  $handler->display->display_options['filters']['field_user_telephone_value']['field'] = 'field_user_telephone_value';
  $handler->display->display_options['filters']['field_user_telephone_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_user_telephone_value']['group'] = 1;
  $handler->display->display_options['filters']['field_user_telephone_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_user_telephone_value']['expose']['operator_id'] = 'field_user_telephone_value_op';
  $handler->display->display_options['filters']['field_user_telephone_value']['expose']['label'] = 'Phone';
  $handler->display->display_options['filters']['field_user_telephone_value']['expose']['operator'] = 'field_user_telephone_value_op';
  $handler->display->display_options['filters']['field_user_telephone_value']['expose']['identifier'] = 'field_user_telephone_value';
  $handler->display->display_options['filters']['field_user_telephone_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    17 => 0,
    18 => 0,
    19 => 0,
    20 => 0,
    21 => 0,
    22 => 0,
    24 => 0,
    23 => 0,
    9 => 0,
    25 => 0,
    26 => 0,
    27 => 0,
    28 => 0,
    29 => 0,
    30 => 0,
    31 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
  );
  /* Filter criterion: User: Mobile (field_user_mobile) */
  $handler->display->display_options['filters']['field_user_mobile_value']['id'] = 'field_user_mobile_value';
  $handler->display->display_options['filters']['field_user_mobile_value']['table'] = 'field_data_field_user_mobile';
  $handler->display->display_options['filters']['field_user_mobile_value']['field'] = 'field_user_mobile_value';
  $handler->display->display_options['filters']['field_user_mobile_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_user_mobile_value']['group'] = 1;
  $handler->display->display_options['filters']['field_user_mobile_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_user_mobile_value']['expose']['operator_id'] = 'field_user_mobile_value_op';
  $handler->display->display_options['filters']['field_user_mobile_value']['expose']['label'] = 'Mobile';
  $handler->display->display_options['filters']['field_user_mobile_value']['expose']['operator'] = 'field_user_mobile_value_op';
  $handler->display->display_options['filters']['field_user_mobile_value']['expose']['identifier'] = 'field_user_mobile_value';
  $handler->display->display_options['filters']['field_user_mobile_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    17 => 0,
    18 => 0,
    19 => 0,
    20 => 0,
    21 => 0,
    22 => 0,
    24 => 0,
    23 => 0,
    9 => 0,
    25 => 0,
    26 => 0,
    27 => 0,
    28 => 0,
    29 => 0,
    30 => 0,
    31 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
  );
  /* Filter criterion: User: Telephone (field_user_telephone) */
  $handler->display->display_options['filters']['field_user_telephone_value_1']['id'] = 'field_user_telephone_value_1';
  $handler->display->display_options['filters']['field_user_telephone_value_1']['table'] = 'field_data_field_user_telephone';
  $handler->display->display_options['filters']['field_user_telephone_value_1']['field'] = 'field_user_telephone_value';
  $handler->display->display_options['filters']['field_user_telephone_value_1']['operator'] = '!=';
  $handler->display->display_options['filters']['field_user_telephone_value_1']['group'] = 2;
  /* Filter criterion: User: Mobile (field_user_mobile) */
  $handler->display->display_options['filters']['field_user_mobile_value_1']['id'] = 'field_user_mobile_value_1';
  $handler->display->display_options['filters']['field_user_mobile_value_1']['table'] = 'field_data_field_user_mobile';
  $handler->display->display_options['filters']['field_user_mobile_value_1']['field'] = 'field_user_mobile_value';
  $handler->display->display_options['filters']['field_user_mobile_value_1']['operator'] = '!=';
  $handler->display->display_options['filters']['field_user_mobile_value_1']['group'] = 2;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'phonebook-users';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Phonebook';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'The phone book enables you to search for people available in the ESS user account directory.

Enter a name or part of name, phone or mobile phone number or part of it e.g. the 4 last digits, then click Apply or press Enter.
Click the <strong>eMail</strong> icon to open a new email to this person.';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['path'] = 'phonebook';
  $export['phonebook_users'] = $view;

  return $export;
}
