<?php
/**
 * @file
 * app_phonebook.ldap_query.inc
 */

/**
 * Implements hook_default_ldap_query().
 */
function app_phonebook_default_ldap_query() {
  $export = array();

  $qid = new stdClass();
  $qid->disabled = FALSE; /* Edit this to true to make a default qid disabled initially */
  $qid->api_version = 1;
  $qid->qid = 'ESS_AD';
  $qid->name = 'ESS AD search';
  $qid->sid = 'ess';
  $qid->status = TRUE;
  $qid->base_dn_str = 'OU=ImportedUsers,DC=esss,DC=lu,DC=se';
  $qid->filter = '(objectClass=user)';
  $qid->attributes_str = 'givenname,sn,title,telephonenumber,mobile,department,mail,manager, thumbnailPhoto,memberof,division,samaccountname';
  $qid->sizelimit = 0;
  $qid->timelimit = 0;
  $qid->deref = FALSE;
  $qid->scope = TRUE;
  $export['ESS_AD'] = $qid;

  return $export;
}
