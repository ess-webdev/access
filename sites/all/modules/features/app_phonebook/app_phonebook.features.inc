<?php
/**
 * @file
 * app_phonebook.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function app_phonebook_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ldap_query" && $api == "ldap_query") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function app_phonebook_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
