<?php
/**
 * @file
 * ess_management_system.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ess_management_system_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
