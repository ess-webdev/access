<?php
/**
 * @file
 * ess_management_system.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ess_management_system_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'app_ess_way';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'ESS Management System';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'ESS Management System - ESSMS (part of ESS Way)';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_chess_url' => 'field_chess_url',
    'field_chess_fileurl' => 'field_chess_fileurl',
    'title' => 'title',
    'field_chess_description' => 'field_chess_description',
    'field_chess_name' => 'field_chess_name',
    'field_chess_author' => 'field_chess_author',
    'field_chess_modified' => 'field_chess_modified',
    'field_chess_classes' => 'field_chess_classes',
    'field_chess_files' => 'field_chess_files',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_chess_url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_fileurl' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_description' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_author' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_modified' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_classes' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_chess_files' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<div class="essms-intro">
<div class="essms-intro-image"><a href="sites/all/modules/features/ess_management_system/ess_way_hq.png" rel="lightbox" title="ESS Management System - ESSMS (ESS Way)" target="_blank"><img src="sites/all/modules/features/ess_management_system/ess_way_lq.png"/></div>
<p>This tool enables you to search for documents published in the <b>ESS Management System</b> in CHESS.
The future tool - ESS Way - is planned to include more documents of general interest for ESS and its collaborators.</p>

<p>You can navigate through documents either by filtering on <b>Document Type</b> or using the free text search fields. The list can be <b>sorted</b> by each column head.</p>

<p>To view preferred PDF-rendition: click the <b>Files</b> link.
To open the document in CHESS: click the <b>CHESS</b> link</p>

<p>If you are searching for specific standards, proceed to <a href="http://online.sis.se" target="_blank">eNav</a>.</p>
</div>
<br/>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'Your search did not match any documents. Please refine your search and try again.';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: url */
  $handler->display->display_options['fields']['field_chess_url']['id'] = 'field_chess_url';
  $handler->display->display_options['fields']['field_chess_url']['table'] = 'field_data_field_chess_url';
  $handler->display->display_options['fields']['field_chess_url']['field'] = 'field_chess_url';
  $handler->display->display_options['fields']['field_chess_url']['label'] = '';
  $handler->display->display_options['fields']['field_chess_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_chess_url']['element_label_colon'] = FALSE;
  /* Field: Content: fileurl */
  $handler->display->display_options['fields']['field_chess_fileurl']['id'] = 'field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['table'] = 'field_data_field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['field'] = 'field_chess_fileurl';
  $handler->display->display_options['fields']['field_chess_fileurl']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_chess_fileurl']['alter']['make_link'] = TRUE;
  /* Field: Content: Display name */
  $handler->display->display_options['fields']['field_chess_display_name']['id'] = 'field_chess_display_name';
  $handler->display->display_options['fields']['field_chess_display_name']['table'] = 'field_data_field_chess_display_name';
  $handler->display->display_options['fields']['field_chess_display_name']['field'] = 'field_chess_display_name';
  $handler->display->display_options['fields']['field_chess_display_name']['label'] = 'Type';
  $handler->display->display_options['fields']['field_chess_display_name']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: name */
  $handler->display->display_options['fields']['field_chess_name']['id'] = 'field_chess_name';
  $handler->display->display_options['fields']['field_chess_name']['table'] = 'field_data_field_chess_name';
  $handler->display->display_options['fields']['field_chess_name']['field'] = 'field_chess_name';
  $handler->display->display_options['fields']['field_chess_name']['label'] = 'Document number';
  $handler->display->display_options['fields']['field_chess_name']['alter']['path'] = '[field_chess_url]';
  $handler->display->display_options['fields']['field_chess_name']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_chess_name']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_chess_name']['element_label_colon'] = FALSE;
  /* Field: Content: revision */
  $handler->display->display_options['fields']['field_chess_revision']['id'] = 'field_chess_revision';
  $handler->display->display_options['fields']['field_chess_revision']['table'] = 'field_data_field_chess_revision';
  $handler->display->display_options['fields']['field_chess_revision']['field'] = 'field_chess_revision';
  $handler->display->display_options['fields']['field_chess_revision']['label'] = 'Rev.';
  $handler->display->display_options['fields']['field_chess_revision']['element_label_colon'] = FALSE;
  /* Field: Content: files */
  $handler->display->display_options['fields']['field_chess_files']['id'] = 'field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['table'] = 'field_data_field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['field'] = 'field_chess_files';
  $handler->display->display_options['fields']['field_chess_files']['label'] = 'Files';
  $handler->display->display_options['fields']['field_chess_files']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_chess_files']['alter']['path'] = 'chess-search/[field_chess_name]/[field_chess_files-value]/latest';
  $handler->display->display_options['fields']['field_chess_files']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_chess_files']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_chess_files']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_chess_files']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_chess_files']['settings'] = array(
    'trim_length' => '40',
  );
  $handler->display->display_options['fields']['field_chess_files']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_chess_files']['separator'] = '<br/>';
  /* Field: Content: description */
  $handler->display->display_options['fields']['field_chess_description']['id'] = 'field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['table'] = 'field_data_field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['field'] = 'field_chess_description';
  $handler->display->display_options['fields']['field_chess_description']['label'] = 'Description';
  $handler->display->display_options['fields']['field_chess_description']['element_class'] = '.narrow-table-column';
  /* Field: Content: author */
  $handler->display->display_options['fields']['field_chess_author']['id'] = 'field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['table'] = 'field_data_field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['field'] = 'field_chess_author';
  $handler->display->display_options['fields']['field_chess_author']['label'] = 'Author';
  /* Field: Content: modified */
  $handler->display->display_options['fields']['field_chess_modified']['id'] = 'field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['table'] = 'field_data_field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['field'] = 'field_chess_modified';
  $handler->display->display_options['fields']['field_chess_modified']['label'] = 'Last modified';
  $handler->display->display_options['fields']['field_chess_modified']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_chess_modified']['settings'] = array(
    'format_type' => 'weather',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'CHESS link';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<img src="sites/all/modules/custom/app_ess_way/chess_orig_0.svg" width="30" hspace="20"/>
';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = '[field_chess_url]';
  $handler->display->display_options['fields']['nothing']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_wrapper_class'] = 'view-table-column-40p';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'chess_file' => 'chess_file',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: type (field_chess_type) */
  $handler->display->display_options['filters']['field_chess_type_value_1']['id'] = 'field_chess_type_value_1';
  $handler->display->display_options['filters']['field_chess_type_value_1']['table'] = 'field_data_field_chess_type';
  $handler->display->display_options['filters']['field_chess_type_value_1']['field'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value_1']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_type_value_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_type_value_1']['expose']['operator_id'] = 'field_chess_type_value_1_op';
  $handler->display->display_options['filters']['field_chess_type_value_1']['expose']['label'] = 'type (field_chess_type)';
  $handler->display->display_options['filters']['field_chess_type_value_1']['expose']['operator'] = 'field_chess_type_value_1_op';
  $handler->display->display_options['filters']['field_chess_type_value_1']['expose']['identifier'] = 'field_chess_type_value_1';
  $handler->display->display_options['filters']['field_chess_type_value_1']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['field_chess_type_value_1']['group_info']['label'] = 'Type';
  $handler->display->display_options['filters']['field_chess_type_value_1']['group_info']['identifier'] = 'field_chess_type_value_1';
  $handler->display->display_options['filters']['field_chess_type_value_1']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'ESS Statute Policy',
      'operator' => '=',
      'value' => 'dmg_ESSStatutePolicy',
    ),
    2 => array(
      'title' => 'Policy',
      'operator' => '=',
      'value' => 'dmg_Policy',
    ),
    3 => array(
      'title' => 'Process',
      'operator' => '=',
      'value' => 'dmg_Process',
    ),
    4 => array(
      'title' => 'Procedure',
      'operator' => '=',
      'value' => 'dmg_OperatingProcedure',
    ),
    5 => array(
      'title' => 'Guideline',
      'operator' => '=',
      'value' => 'dmg_Guideline',
    ),
    6 => array(
      'title' => 'Template',
      'operator' => '=',
      'value' => 'dmg_Template',
    ),
    7 => array(
      'title' => 'Rules',
      'operator' => '=',
      'value' => 'dmg_Rules',
    ),
  );
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'allwords';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: name (field_chess_name) */
  $handler->display->display_options['filters']['field_chess_name_value']['id'] = 'field_chess_name_value';
  $handler->display->display_options['filters']['field_chess_name_value']['table'] = 'field_data_field_chess_name';
  $handler->display->display_options['filters']['field_chess_name_value']['field'] = 'field_chess_name_value';
  $handler->display->display_options['filters']['field_chess_name_value']['operator'] = 'allwords';
  $handler->display->display_options['filters']['field_chess_name_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['operator_id'] = 'field_chess_name_value_op';
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['label'] = 'Document number';
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['operator'] = 'field_chess_name_value_op';
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['identifier'] = 'field_chess_name_value';
  $handler->display->display_options['filters']['field_chess_name_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: description (field_chess_description) */
  $handler->display->display_options['filters']['field_chess_description_value']['id'] = 'field_chess_description_value';
  $handler->display->display_options['filters']['field_chess_description_value']['table'] = 'field_data_field_chess_description';
  $handler->display->display_options['filters']['field_chess_description_value']['field'] = 'field_chess_description_value';
  $handler->display->display_options['filters']['field_chess_description_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_chess_description_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_description_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['operator_id'] = 'field_chess_description_value_op';
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['label'] = 'Description';
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['operator'] = 'field_chess_description_value_op';
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['identifier'] = 'field_chess_description_value';
  $handler->display->display_options['filters']['field_chess_description_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: author (field_chess_author) */
  $handler->display->display_options['filters']['field_chess_author_value']['id'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['table'] = 'field_data_field_chess_author';
  $handler->display->display_options['filters']['field_chess_author_value']['field'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['operator'] = 'allwords';
  $handler->display->display_options['filters']['field_chess_author_value']['group'] = 1;
  $handler->display->display_options['filters']['field_chess_author_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['operator_id'] = 'field_chess_author_value_op';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['label'] = 'Author';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['operator'] = 'field_chess_author_value_op';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['identifier'] = 'field_chess_author_value';
  $handler->display->display_options['filters']['field_chess_author_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: type (field_chess_type) */
  $handler->display->display_options['filters']['field_chess_type_value']['id'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value']['table'] = 'field_data_field_chess_type';
  $handler->display->display_options['filters']['field_chess_type_value']['field'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value']['value'] = 'dmg_ESSStatutePolicy';
  $handler->display->display_options['filters']['field_chess_type_value']['group'] = 2;
  /* Filter criterion: Content: type (field_chess_type) */
  $handler->display->display_options['filters']['field_chess_type_value_7']['id'] = 'field_chess_type_value_7';
  $handler->display->display_options['filters']['field_chess_type_value_7']['table'] = 'field_data_field_chess_type';
  $handler->display->display_options['filters']['field_chess_type_value_7']['field'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value_7']['value'] = 'dmg_Rules';
  $handler->display->display_options['filters']['field_chess_type_value_7']['group'] = 2;
  /* Filter criterion: Content: type (field_chess_type) */
  $handler->display->display_options['filters']['field_chess_type_value_6']['id'] = 'field_chess_type_value_6';
  $handler->display->display_options['filters']['field_chess_type_value_6']['table'] = 'field_data_field_chess_type';
  $handler->display->display_options['filters']['field_chess_type_value_6']['field'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value_6']['value'] = 'dmg_Template';
  $handler->display->display_options['filters']['field_chess_type_value_6']['group'] = 2;
  /* Filter criterion: Content: type (field_chess_type) */
  $handler->display->display_options['filters']['field_chess_type_value_2']['id'] = 'field_chess_type_value_2';
  $handler->display->display_options['filters']['field_chess_type_value_2']['table'] = 'field_data_field_chess_type';
  $handler->display->display_options['filters']['field_chess_type_value_2']['field'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value_2']['value'] = 'dmg_OperatingProcedure';
  $handler->display->display_options['filters']['field_chess_type_value_2']['group'] = 2;
  /* Filter criterion: Content: type (field_chess_type) */
  $handler->display->display_options['filters']['field_chess_type_value_3']['id'] = 'field_chess_type_value_3';
  $handler->display->display_options['filters']['field_chess_type_value_3']['table'] = 'field_data_field_chess_type';
  $handler->display->display_options['filters']['field_chess_type_value_3']['field'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value_3']['value'] = 'dmg_Policy';
  $handler->display->display_options['filters']['field_chess_type_value_3']['group'] = 2;
  /* Filter criterion: Content: type (field_chess_type) */
  $handler->display->display_options['filters']['field_chess_type_value_4']['id'] = 'field_chess_type_value_4';
  $handler->display->display_options['filters']['field_chess_type_value_4']['table'] = 'field_data_field_chess_type';
  $handler->display->display_options['filters']['field_chess_type_value_4']['field'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value_4']['value'] = 'dmg_Process';
  $handler->display->display_options['filters']['field_chess_type_value_4']['group'] = 2;
  /* Filter criterion: Content: type (field_chess_type) */
  $handler->display->display_options['filters']['field_chess_type_value_5']['id'] = 'field_chess_type_value_5';
  $handler->display->display_options['filters']['field_chess_type_value_5']['table'] = 'field_data_field_chess_type';
  $handler->display->display_options['filters']['field_chess_type_value_5']['field'] = 'field_chess_type_value';
  $handler->display->display_options['filters']['field_chess_type_value_5']['value'] = 'dmg_Guideline';
  $handler->display->display_options['filters']['field_chess_type_value_5']['group'] = 2;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'ess-management-system';
  $handler->display->display_options['menu']['title'] = 'ess management system';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $export['app_ess_way'] = $view;

  return $export;
}
