/**
 * @file
 * Handles global javascript behavior.
 */

(function($) {

// Create attach and detach behaviors.
Drupal.behaviors.accessGlobal = {
  attach: function(context, settings) {
    if (!settings.access) {
      return;
    }

    // Trigger different actions depending on which breakpoint we're entering.
    switch (settings.access.breakpoint) {
      case 'small':
        Drupal.behaviors.resetLayout();
        $('body').addClass('small');
        Drupal.behaviors.shrinkLayout();
        break;
      case 'fit':
        Drupal.behaviors.resetLayout();
        $('body').addClass('fit');
        Drupal.behaviors.shrinkLayout();
        $('#content-wrapper').removeAttr('style');
        break;
      case 'menu-switch':
        Drupal.behaviors.resetLayout();
        $('body').addClass('menu-switch');
        Drupal.behaviors.shrinkLayout();
        $('#content-wrapper').removeAttr('style');
        break;
      case 'medium':
        $('body').addClass('medium');
        Drupal.behaviors.resetLayout();
        break;
      case 'large':
        $('body').addClass('large');
        Drupal.behaviors.resetLayout();
        break;
    }
  },

  detach: function(context, settings) {
    if (!settings.access) {
      return;
    }

    // Trigger different actions depending on which breakpoint we're leaving.
    switch (settings.access.breakpoint) {
      case 'small':
        $('body').removeClass('small');
        Drupal.behaviors.resetLayout();
        break;
      case 'fit':
        $('body').removeClass('fit');
        Drupal.behaviors.resetLayout();
        break;
      case 'menu-switch':
        $('body').removeClass('menu-switch');
        Drupal.behaviors.resetLayout();
        break;
      case 'medium':
        $('body').removeClass('medium');
        Drupal.behaviors.resetLayout();
        break;
      case 'large':
        $('body').removeClass('large');
        break;
    }
  }
};

/**
 * Function to layout the menu to a smaller screen.
 */
Drupal.behaviors.shrinkLayout = function () {
	$('body').append($('<div id="nav"></div>'));
  $('#header').append($('<div class="nav-button-wrapper"><div class="nav-button"><span></span></div></div>'));
	$('#nav').append($('#apps > ul.menu').clone().attr('id', 'nav-menu'));
	$('#nav').append($('ul.access-utility-links').clone().attr('id', 'nav-utility-links'));
	$('#nav').append($('#block-views-exp-search-page').clone().attr('class', 'nav-search'));
  Drupal.access.menu();
};

Drupal.behaviors.resetLayout = function() {
  $('#nav').remove();
  $('.nav-button-wrapper').remove();
  $('body').removeClass('open');
};

// Custom namespace for our functions and variables.
Drupal.access = Drupal.access ? Drupal.access : {};

// Function that handles the group teaser.
Drupal.access.groupTeaser = function() {
  var $teaser = $('.node-group.view-mode-teaser');
  var $description = $teaser.find('.description');
  var $titleLink = $teaser.find('.title a');

  $description.each(function() {
    var height = $(this).outerHeight();
    $(this).css({'margin-bottom': 0 - height + 'px'});
  });

  $titleLink.bigBang({
    selector: '.node-group'
  });

  $teaser.each(function() {
    var ints = $(this).find('.title').outerHeight() + $(this).find('.description').outerHeight() + 84;

    if ($(this).is('.master') || $(this).is('.global')) {

      if (ints < 215) {
        ints = 215;
      }
    }

    $(this).height(ints);
  });
};

// Function to handle menu click
Drupal.access.menu = function() {
  $('.nav-button-wrapper').click(function(){
    $('#nav').toggleClass('open');
    $('body').toggleClass('open');
  });

  // Close the menu if users press the content surface
  $('#main-wrapper').click(function(){
    if($('.sidebar-first').hasClass('open')){
      $('.sidebar-first').toggleClass('open');
      $('.content').toggleClass('open');
    }
  });
};

// Bigbang front slider.
Drupal.access.clickableSlider = function() {
  var links = $('#article-list div.node .title a');
  $.each(links, function( index, value ) {
    var div = $(value).closest('.node-news');
    $(value).bigBang({
      selector: div
    });
  });
};

//Function that clones the Table of Contents to desired location in group-right (Wiki).
Drupal.access.throbber = function() {
  $('.facetapi-facetapi-checkbox-links label').click(function(){
    $('#throbber').addClass('active');
    $('#loading').addClass('active');
    $('#throbber').click(function(){
      $(this).removeClass('active');
      $('#loading').removeClass('active');
    });
  });
};

// Function that handles the event small-teaser.
Drupal.access.eventSmallTeaser = function() {
  var $teaser = $('.node-event.view-mode-small_teaser');
  var $titleLink = $teaser.find('.title a');

  $titleLink.bigBang({
    selector: '.node-event'
  });
};

// Function that handles Relevant article teasers.
Drupal.access.relevantArticleTeaser = function() {
  var $teaser = $('.node-relevant-articles.view-mode-teaser');
  var $titleLink = $teaser.find('.title a');

  $titleLink.bigBang({
    selector: '.node-relevant-articles'
  });
};

// Function that handles the news teaser.
Drupal.access.newsTeaser = function() {
  var $target = $('.node-news.view-mode-teaser .image');
  $target.imgLiquid();
};

// Function that handles the frontpage news ticker.
Drupal.access.newsTicker = function() {
  var $target = $('#featured-articles #article-list');
  if($target.children().length > 1) {
    $target.bxSlider({
      auto: true,
      preventDefaultSwipeY: false,
      controls: false,
      pager: true,
      responsive: true,
      mode: 'horizontal',
      useCSS: false
    });
  }
}
// Function that toggles the comment form.
Drupal.access.commentToggler = function() {
  var $trigger = $('h2#comments-heading span.add-button');
  var $target = $('#add-comment-form');

  $trigger.click(function() {
    $target.slideToggle();
  });
};

// Function that adds esthetics to the Table of Contents(Wiki).
Drupal.access.tocClasses = function() {
  var $target = $('#toc ul li');

  $target.each(function() {
    if ($(this).children('ul').length > 0) {
      $(this).addClass('parent');
    }
    else {
      $(this).addClass('child');
    }
  });
};

// Function that clones the Table of Contents to desired location in group-right (Wiki).
Drupal.access.tocClone = function() {
  var $object = $('#toc');
  var $target = $('.node-wiki.view-mode-full .group-right');
  var $clone = $object.clone();
  $object.hide();
  $target.append($clone);
};

Drupal.access.stripMangerText = function() {
  searchText = $('.field-name-field-user-manager').closest('div').find('.field-item').text();
  if(typeof searchText !== 'undefined'){
    var texts = searchText.split(',');
    $(texts).each(function(index, text) {
      if(text.indexOf('CN=') >= 0){
        var name = text.replace('CN=', "");
        $('.field-name-field-user-manager').closest('div').find('.field-item').text(name);
      }
    });
  }
};

// Function that adds esthetics to the Wiki contents block (Wiki).
Drupal.access.wikiTreeClasses = function() {
  var $target = $('#wiki-tree ul li');

  $target.each(function() {
    if ($(this).children('ul').length > 0) {
      $(this).addClass('parent');
    }
    else {
      $(this).addClass('child');
    }
  });
};

Drupal.access.hideCheckboxes = function() {
  var checkboxes = $('#edit-og-user-node1-und-0-default input');
  $.each(checkboxes, function(index, value) {
    $(value).hide();
  });
}

// Function that shows add post list.
Drupal.access.expandPostAdd = function() {
  $('#quicktabs-access_bw #all .top-wrapper > a.access-posts-create').click(function(event) {
    event.preventDefault();
    $('.access-bw-create-choose-holder').slideToggle();
  });
}

// Run whenever the DOM tree is changed, e.g. through AJAX/AHAH
Drupal.behaviors.access = {
  attach: function (context, settings) {
    $("select").uniform();

  }
};

// Function that adds superLabels to form elements
Drupal.access.superLabels = function() {
	// Superlabels on login form
  $('#block-views-exp-search-page form').superLabels({
    labelLeft: 13,
    labelTop: 7
  });

  $('#nav-search').superLabels({
    labelLeft: 13,
    labelTop: 7
  });
};

// Function that aligns the height of one object to the other
Drupal.access.alignHeight = function() {
  // alignHeight on sidebar on profile pages
  if ($('body').hasClass('page-user')) {
    if ($('.group-left').height() < $('.group-middle').height()){
      $('.group-left').height($('.group-middle').height() + 40);
    }

    if ($('.user.user-full .group-left').height() < $('.user.user-full .group-right').height()){
      $('.user.user-full .group-left').height($('.user.user-full .group-right').height() + 40);
     }
  }
};

// Run once when the DOM is ready (page load)
$(document).ready(function() {

  Drupal.access.throbber();
	Drupal.access.groupTeaser();
	Drupal.access.eventSmallTeaser();
	Drupal.access.relevantArticleTeaser();
	Drupal.access.newsTeaser();
  Drupal.access.clickableSlider();
  Drupal.access.expandPostAdd();
	Drupal.access.commentToggler();
	Drupal.access.newsTicker();
  Drupal.access.stripMangerText();
  Drupal.access.hideCheckboxes();
	Drupal.access.tocClasses();
	Drupal.access.tocClone();
	Drupal.access.wikiTreeClasses();
	Drupal.access.superLabels();
	Drupal.access.alignHeight();

	$('#throbber').hover(function(){
    $(this).addClass('active');
	});
	$('#loading').hover(function(){
    $('#throbber').removeClass('active');
	});

});

// Run when the window is resized.
$(window).resize(function() {

  Drupal.access.groupTeaser();

});

})(jQuery);
