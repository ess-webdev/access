/**
 * @file
 * Initializes the global javascript settings for the site.
 */

// Create our namespace within the settings namespace.
Drupal.settings.access = Drupal.settings.access ? Drupal.settings.access : {};

(function($) {

// Setup the jRespond breakpoints.
var jRes = jRespond([
  {
    label: 'small',
    enter: 0,
    exit: 445
  },
  {
    label: 'fit',
    enter: 446,
    exit: 800
  },
  {
    label: 'menu-switch',
    enter: 801,
    exit: 840
  },
  {
    label: 'medium',
    enter: 841,
    exit: 1080
  },
  {
    label: 'large',
    enter: 1081,
    exit: 10000
  }
]);

// Add a function to jRespond that fires for every breakpoint.
$(document).ready(function() {
  jRes.addFunc({
    breakpoint: '*',
    exit: function() {
      // Detach behaviors when exiting a breakpoint.
      Drupal.detachBehaviors();
    },
    enter: function() {
      // Update the current breakpoint settings variable, and attach behaviors.
      Drupal.settings.access.breakpoint = jRes.getBreakpoint();
      Drupal.attachBehaviors();
    },
  });
});

})(jQuery);
