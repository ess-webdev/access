<?php

/**
 * @file
 * Display Suite 2 column template.
 */
?>
<!-- ds layout wrapper -->
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-2col <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <!-- left -->
  <?php if (!empty($left)): ?>
    <<?php print $left_wrapper ?> class="group-left<?php print $left_classes; ?>">
      <?php print $left; ?>
    </<?php print $left_wrapper ?>>
  <?php endif; ?>
  <!-- /left -->

  <!-- right -->
  <?php if (!empty($right)): ?>
    <<?php print $right_wrapper ?> class="group-right<?php print $right_classes; ?>">
      <?php print $right; ?>
    </<?php print $right_wrapper ?>>
  <?php endif; ?>
  <!-- /right -->

</<?php print $layout_wrapper ?>>
<!-- /ds layout wrapper -->
