<?php

/**
 * @file
 * Display Suite 3 column 25/50/25 template.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-3col <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

<div class="main-wrapper">
  <<?php print $left_wrapper ?> class="group-left<?php print $left_classes;?>">
    <?php print $left; ?>
  </<?php print $left_wrapper ?>>

  <<?php print $middle_wrapper ?> class="group-middle<?php print $middle_classes;?>">

    <!-- Print a link for being done editing -->
    <?php print l('Done editing','user/' . arg(1), array('attributes' => array('class' => array('button','edit edit-done'))));?>

    <?php print $middle; ?>

    <?php if (!empty($drupal_render_children)): ?>
      <?php print $drupal_render_children ?>
    <?php endif; ?>
  </<?php print $middle_wrapper ?>>
</div>

  <<?php print $right_wrapper ?> class="group-right<?php print $right_classes;?>">
    <?php print $right; ?>
  </<?php print $right_wrapper ?>>

</<?php print $layout_wrapper ?>>
