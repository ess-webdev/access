<?php

/**
 * @file
 * Display Suite 2 column stacked template.
 * This is the default template.
 */
?>
<!-- ds layout wrapper -->
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-2col-stacked <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <!-- header -->
  <?php if (!empty($header)): ?>
    <<?php print $header_wrapper ?> class="group-header<?php print $header_classes; ?>">
      <?php print $header; ?>
    </<?php print $header_wrapper ?>>
  <?php endif; ?>
  <!-- /header -->

  <!-- left -->
  <?php if (!empty($left)): ?>
    <<?php print $left_wrapper ?> class="group-left<?php print $left_classes; ?>">
      <?php print $left; ?>
    </<?php print $left_wrapper ?>>
  <?php endif; ?>
  <!-- /left -->

  <!-- right -->
  <?php if (!empty($right)): ?>
    <<?php print $right_wrapper ?> class="group-right<?php print $right_classes; ?>">
      <?php print $right; ?>
    </<?php print $right_wrapper ?>>
  <?php endif; ?>
  <!-- /right -->

  <!-- footer -->
  <?php if (!empty($footer)): ?>
    <<?php print $footer_wrapper ?> class="group-footer<?php print $footer_classes; ?>">
      <?php print $footer; ?>
    </<?php print $footer_wrapper ?>>
  <?php endif; ?>
  <!-- /footer -->

</<?php print $layout_wrapper ?>>
<!-- /ds layout wrapper -->

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
