<?php

/**
 * @file
 * Display Suite 2 column stacked template.
 * Specific for Event nodes - View mode full.
 */
?>
<!-- ds layout wrapper -->
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-2col-stacked <?php print $classes;?> clearfix">

  <!-- left -->
  <?php if (!empty($left)): ?>
    <<?php print $left_wrapper ?> class="group-left<?php print $left_classes; ?>">
      <div class="pre-content">
        <?php print render($content['field_logo']); ?>
        <?php print render($content['field_date']); ?>
      </div>
      <?php print $left; ?>
    </<?php print $left_wrapper ?>>
  <?php endif; ?>
  <!-- /left-->

  <!-- right -->
  <?php if (!empty($right)): ?>
    <<?php print $right_wrapper ?> class="group-right<?php print $right_classes; ?>">
      <?php print render($content['changed_date']); ?>
      <?php print render($content['ds_flag_access_content_like']); ?>
      <?php print render($content['content_like_count']); ?>
      <div class="location">
        <div class="label"><?php print t('Location'); ?></div>
        <?php print render($content['field_address']); ?>
        <div class="url">
          <?php
            $object = entity_metadata_wrapper('node',arg(1));
            $location_url = $object->field_offsite_url->value();
            print l($location_url,$location_url,array('attributes' => array('target' => '_blank')));
          ?>
        </div>
      </div>

      <!-- topics -->
      <?php print render($content['field_topics']); ?>
      <!-- /topics -->

    </<?php print $right_wrapper ?>>
  <?php endif; ?>
  <!-- /right -->

  <!-- footer -->
  <?php if (!empty($footer)): ?>
    <<?php print $footer_wrapper ?> class="group-footer<?php print $footer_classes; ?>">
      <?php print $footer ?>
    </<?php print $footer_wrapper ?>>
  <?php endif; ?>

</<?php print $layout_wrapper ?>>
<!-- /ds layout wrapper -->

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
