<?php
/**
 * @file
 * Default theme implementation that displays a node using the DS 1 column
 * layout.
 *
 * If you'd like to create a different template file for nodes, you should
 * create a new DS layout, and place it in the templates/ds folder, just like
 * this implementation.
 *
 * Have a look at the example_layout folder in the DS folder for guidance.
 */
?>
<div class="<?php print $classes; ?> <?php print $ds_content_classes; ?> contextual-links-region">
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="group-teaser-header">
  	<?php print render($content['ds_flag_commons_follow_group']); ?>
  	<?php print render($content['commons_contributors_group_entity_view_1']); ?>
  </div>

  <div class="group-content">
  	<?php print render($content['title']); ?>
  	<?php print render($content['body']); ?>
	</div>
</div>