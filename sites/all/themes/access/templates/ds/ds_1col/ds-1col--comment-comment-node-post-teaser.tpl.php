<?php
/**
 * @file
 * Default theme implementation that displays a node using the DS 1 column
 * layout.
 *
 * If you'd like to create a different template file for nodes, you should
 * create a new DS layout, and place it in the templates/ds folder, just like
 * this implementation.
 *
 * Have a look at the example_layout folder in the DS folder for guidance.
 */
?>
<div class="<?php print $classes; ?> <?php print $ds_content_classes; ?> contextual-links-region">
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>
  <div class="comment comment-teaser">
    <?php if ($content): ?>
      <div class="author">
        <span class="comment-author"> <?php print render($content['author']); ?> </span>
        <span class="commented-on"> commented on </span>
        <span class="comment-title"> <?php print render($content['post_link']); ?> </span>
      </div>
      <div class="comment-main">
        <p class="comment-body"><span>"</span> <?php print render($content['comment_body']); ?> <span>"</span></p>
        <span class="read-more-comment-url"> <?php print render($content['read_more_url']); ?> </span>
      </div>
    <?php endif; ?>
  </div>

</div>
