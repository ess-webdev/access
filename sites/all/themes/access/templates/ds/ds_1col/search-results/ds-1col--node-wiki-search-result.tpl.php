<?php
/**
 * @file
 * Default theme implementation that displays a node using the DS 1 column
 * layout.
 *
 * If you'd like to create a different template file for nodes, you should
 * create a new DS layout, and place it in the templates/ds folder, just like
 * this implementation.
 *
 * Have a look at the example_layout folder in the DS folder for guidance.
 */
?>
<div class="<?php print $classes; ?> <?php print $ds_content_classes; ?> contextual-links-region">
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="header">
    <?php print render($content['title']); ?>
    <?php print render($content['field_topics']); ?>
  </div>
  <?php print render($content['body']); ?>

  <div class="footer">
    <div class="posted">
      <?php print render($content['author']); ?>
      <?php print render($content['post_date']); ?>
    </div>
    <?php print render($content['og_group_ref']); ?>
  </div>
</div>
