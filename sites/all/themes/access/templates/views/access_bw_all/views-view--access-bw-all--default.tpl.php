<?php
/**
 * @file
 * Main view template.
 *
 * If you'd like to override this template, you should create a folder within
 * this folder and name it to the machine-readable name of the view that you're
 * overriding. Then have a look at the theme information for that view, and
 * you'll be able to see which naming convention you should use for the new
 * file.
 *
 * You might end up with something like view_name/views-view--view-name.tpl.php.
 */
?>
<div id="all" class="<?php print $classes; ?>">
  <?php print $header; ?>
  <div class="top-wrapper">
	  <?php print $exposed; ?>
	  <?php print $attachment_before; ?>
	</div>
	<div class="list-content">
	  <?php print $rows; ?>
	</div>
	<div class="list-empty">
  	<?php print $empty; ?>
  </div>
  <div class="pager">
	  <?php print $pager; ?>
	</div>
  <?php print $attachment_after; ?>
  <?php print $more; ?>
  <?php print $footer; ?>
</div>