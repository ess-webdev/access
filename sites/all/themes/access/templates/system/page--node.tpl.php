<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * This file will print the entire contents within the <body> tag. The only
 * variables you should be printing, should be the available regions. The
 * contents within the regions should be added using Contexts and blocks, and
 * therefore, shouldn't be added here.
 *
 * Feel free to add any HTML that you'd like to use for creating the structure
 * of the page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>
<div id="header">
	<?php print render($page['header']); ?>
</div>

<div id="main-wrapper" class="barrier">
	<div id="content-wrapper" class="limiter">

    <div id="console" class="clearfix">
      <?php print $messages; ?>
    </div>

	<?php if ($page['sub_header']): ?>
		<?php print render($page['sub_header']); ?>
	<?php endif; ?>
	<?php print render($page['content']); ?>
	</div>
</div>
<?php print render($page['footer']); ?>
