<?php
/**
 * @file
 * This file should contain any preprocess functions that are being used.
 *
 * Preprocess functions are functions that runs before the theme function or
 * template file. For example, the template_preprocess_node will process the
 * variables that will be available to the node.tpl.php.
 *
 * You are also able to manage the template suggestions using these functions.
 * Using the node example again, the node.tpl.php will cover any node, but
 * node-123.tpl.php will only cover the node with the id 123. This is a simple
 * template suggestion, and you could add whatever suggestions you like.
 *
 * @see http://drupal.org/node/223430
 */

/**
 * Implements template_preprocess_block().
 */
function access_preprocess_block(&$variables) {
  // Alter the title for some blocks. This is manually configured here, because
  // Features doesn't export block configuration.
  switch ($variables['block']->bid) {
    case 'system-main-menu':
      $variables['block']->subject = '';
      break;
  }
}

/**
 * Implements template_preprocess_page().
 */
function access_preprocess_page(&$variables) {
  // Add template suggestions based on content type.
  if (isset($variables['node'])) {
    $variables['theme_hook_suggestions'][] = "page__type__" . $variables['node']->type;
  }
}

/**
 * Implements template_preprocess_node().
 */
function access_preprocess_node(&$variables) {
  $elements = $variables['elements'];

  // Check if the current node is a post with view mode teaser.
  if ($elements['#bundle'] == 'post' && $elements['#view_mode'] == 'teaser') {

    // Pass the node to entity metadata wrapper.
    $wrapper = entity_metadata_wrapper('node', $elements['#node']);

    // Get the tag.
    $tag = $wrapper->field_post_tag->value()->name;

    // If the post is tagged. Add classes.
    if ($tag) {
      $tag = str_replace(' ', '-', $tag);
      $variables['classes_array'][] = 'has-tag';
      $variables['classes_array'][] = strtolower($tag);
    }
  }

  if ($variables['type'] == 'group') {
    $group_global = $variables['field_group_global'];
    $group_type = $variables['field_head_sub_group'];
    $group_access = $variables['group_access'];

    if ($group_type['und'][0]['value'] == FALSE && $group_global['und'][0]['value'] != '1' && $group_access['und'][0]['value'] != '1') {

      $variables['classes_array'][] = 'master';
    }
    elseif ($group_type['und'][0]['value'] == '1' && $group_global['und'][0]['value'] != '1') {
      $variables['classes_array'][] = 'minion';
    }
    if ($group_global['und'][0]['value'] == '1') {
      $variables['classes_array'][] = 'global';
    }
    if ($group_access['und'][0]['value'] == '1') {
      $variables['classes_array'][] = 'private';
    }
  }
}

/**
 * Implements template_preprocess_comment().
 */
function access_preprocess_comment(&$variables) {
  $elements = $variables['elements'];
  if ($elements['#bundle'] == 'comment_node_post') {
    $nid = $elements['#comment']->nid;
    $node = node_load($nid);
    $variables['content']['read_more_url'] = l(t('Read more'), 'node/' . $nid);
    $variables['content']['post_link'] = l($node->title, 'node/' . $nid);
  }
}

/**
 * Implements template_preprocess_blockify_logo().
 */
function access_preprocess_blockify_logo(&$variables) {
  // Change the url of the logo to the graphics folder.
  $variables['logo_path'] = file_create_url(drupal_get_path('theme', 'access') . '/graphics/logo.png');
}

/**
 * Implements template_preprocess_blockify_page_title().
 */
// function access_preprocess_blockify_page_title(&$variables) {
//   global $user;

//   if (!$user->uid) {
//     return;
//   }

//   if (drupal_is_front_page()) {
//     $user_object = user_load($user->uid);

//     $variables['page_title'] = $variables['page_title'] . ', <span class="name">' . $user_object->realname . '</span>';
//   }
// }

/**
 * Implements template_preprocess_field().
 */
function access_preprocess_field(&$variables) {
  if ($variables['element']['#view_mode'] == 'search_result' && $variables['element']['#field_name'] == 'title') {
    $variables['items'][0]['#markup'] .= '<span class="content_type">' . $variables['element']['#bundle'] . '</span>';
  }

  // Add comments count to a ds field. This will be visible on post teasers.
  if ($variables['element']['#view_mode'] == 'teaser' && $variables['element']['#bundle'] == 'post') {
    // Get the nid for the current node.
    $nid = $variables['element']['#object']->nid;

    // Create a new entityFieldQuery object and get the comment count.
    $query = new entityFieldQuery();
    $query->entityCondition('entity_type', 'comment')
      ->propertyCondition('status', 1)
      ->propertyCondition('nid', $nid, '=');

    $result = $query->count()->execute();

    // Add the value to the dispaly suite field.
    $variables['element']['comment_count'] = $result;
  }
}
