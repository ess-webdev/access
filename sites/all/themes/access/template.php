<?php
/**
 * @file
 * Main PHP file for this theme.
 *
 * This will contain any hooks and custom functions that are being used for this
 * theme. It's also responsible for loading the preprocess.inc and theme.inc
 * files.
 */

/**
 * Implements hook_html_head_alter().
 */
function access_html_head_alter(&$head_elements) {
  // Add a tag that disables the compatibility mode in IE.
  $head_elements['disable_compatibility_mode'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=Edge',
    ),
  );
}

/**
 * Implements hook_css_alter().
 */
function access_css_alter(&$css) {
  // Specify the CSS files that we want to exclude.
  $exclude = array(
    'misc/ui/jquery.ui.core.css',
    'misc/ui/jquery.ui.theme.css',
    'misc/ui/jquery.ui.resizable.css',
    'misc/ui/jquery.ui.button.css',
    'misc/ui/jquery.ui.dialog.css',
    'modules/system/system.menus.css',
    'modules/system/system.messages.css',
    'modules/system/system.theme.css',
    'modules/node/node.css',
    'modules/user/user.css',
    'modules/field/theme/field.css',
    'sites/all/modules/contrib/taskbar/styles/base/taskbar_base.css',
    'sites/all/modules/contrib/taskbar/styles/bottom/taskbar_bottom.css',
    'sites/all/modules/contrib/taskbar/styles/default/taskbar_default.css'
  );

  // Iterate through each excluded CSS file, and remove it from the loaded
  // files.
  foreach ($exclude as $file) {
    unset($css[$file]);
  }
}
/**
 * Implements hook_css_alter().
 */
function access_js_alter(&$js) {

  // Replace the jQuery version that's provided bu jQuery update with a version
  // of our choise.
  //$js['https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'] = $js['https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js'];
  //$js['https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js']['data'] = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';
  //$js['https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js']['version'] = '1.7.2';
  //unset($js['https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js']);

  // Specify the JS files that we want to exclude.
  $exclude = array(
    'sites/all/modules/contrib/taskbar/taskbar.js'
  );

  // Iterate through each excluded CSS file, and remove it from the loaded
  // files.
  foreach ($exclude as $file) {
    unset($js[$file]);
  }

 /* KMY: Disabled the above section - using Jquery Multi is perhaps better... */


}

// Include the preprocess and theme files. These includes preprocess
// implementations and theme overrides.
include_once 'preprocess.inc';
include_once 'theme.inc';

