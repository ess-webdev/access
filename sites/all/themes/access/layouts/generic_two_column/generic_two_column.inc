<?php

// Plugin definition
$plugin = array(
  'title' => t('Generic two column'),
  'category' => t('Columns: 2'),
  'icon' => 'generic_two_column.png',
  'theme' => 'generic_two_column',
  'css' => 'generic_two_column.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
