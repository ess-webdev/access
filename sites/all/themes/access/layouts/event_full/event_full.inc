<?php

// Plugin definition
$plugin = array(
  'title' => t('Event-full'),
  'category' => t('Columns: 2'),
  'icon' => 'event_full.png',
  'theme' => 'event_full',
  'css' => 'event_full.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
