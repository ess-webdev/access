<?php

// Plugin definition
$plugin = array(
  'title' => t('Post-full'),
  'category' => t('Columns: 2'),
  'icon' => 'post_full.png',
  'theme' => 'post_post',
  'css' => 'post_full.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
