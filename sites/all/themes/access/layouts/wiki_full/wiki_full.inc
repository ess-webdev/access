<?php

// Plugin definition
$plugin = array(
  'title' => t('Wiki-full'),
  'category' => t('Columns: 2'),
  'icon' => 'wiki_full.png',
  'theme' => 'wiki_full',
  'css' => 'wiki_full.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
