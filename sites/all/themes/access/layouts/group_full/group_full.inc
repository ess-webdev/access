<?php

// Plugin definition
$plugin = array(
  'title' => t('Group full'),
  'category' => t('Columns: 3'),
  'icon' => 'group_full.png',
  'theme' => 'group_full',
  'css' => 'group_full.css',
  'regions' => array(
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side')
  ),
);

