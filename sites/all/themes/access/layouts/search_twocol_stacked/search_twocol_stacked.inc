<?php

// Plugin definition
$plugin = array(
  'title' => t('Search two column stacked'),
  'category' => t('Columns: 2'),
  'icon' => 'search_twocol_stacked.png',
  'theme' => 'panels-search-twocol-stacked',
  'css' => 'search_twocol_stacked.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
