<?php

// Plugin definition
$plugin = array(
  'title' => t('Document-full'),
  'category' => t('Columns: 2'),
  'icon' => 'document_full.png',
  'theme' => 'document_full',
  'css' => 'document_full.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
