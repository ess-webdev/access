<?php

// Plugin definition
$plugin = array(
  'title' => t('Dashboard two column'),
  'category' => t('Columns: 2'),
  'icon' => 'dashboard_two_column.png',
  'theme' => 'dashboard_two_column',
  'css' => 'dashboard_two_column.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
